import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ControlModule } from './control/control.module';
import { OverviewModule } from './overview/overview.module';

@NgModule({
  imports: [
    CommonModule,
    ControlModule,
    OverviewModule
  ]
})
export class GameModule { }
