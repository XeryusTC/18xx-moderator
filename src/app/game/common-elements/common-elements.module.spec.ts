import { CommonElementsModule } from './common-elements.module';

describe('CommonElementsModule', () => {
  let commonElementsModule: CommonElementsModule;

  beforeEach(() => {
    commonElementsModule = new CommonElementsModule();
  });

  it('should create an instance', () => {
    expect(commonElementsModule).toBeTruthy();
  });
});
