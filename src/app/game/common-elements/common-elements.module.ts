import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { CompanyColorsDirective } from './company-colors/company-colors.directive';
import { LogEntryComponent } from './log-entry/log-entry.component';
import { AbsPipe } from './abs.pipe';

@NgModule({
  imports: [
    CommonModule,
    FontAwesomeModule
  ],
  declarations: [
    CompanyColorsDirective,
    LogEntryComponent,
    AbsPipe
  ],
  exports: [
    CompanyColorsDirective,
    LogEntryComponent
  ]
})
export class CommonElementsModule { }
