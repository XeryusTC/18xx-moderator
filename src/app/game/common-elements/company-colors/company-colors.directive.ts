import { Directive, ElementRef, Input } from '@angular/core';
import { Company } from '../../../models/state';

@Directive({
  selector: '[appCompanyColors]'
})
export class CompanyColorsDirective {
  _company;
  _noSpacing = false;

  @Input('appCompanyColors')
  set companyColors(company: Company) {
    this._company = company;
    this.updateStyling();
  }

  @Input('noSpacing')
  set noSpacing(v: boolean) {
    this._noSpacing = !!v;
    this.updateStyling();
  }

  private updateStyling() {
    if (this._company) {
      this.el.nativeElement.style.color = this._company.textColor;
      this.el.nativeElement.style.backgroundColor = this._company.backgroundColor;
      if (!this._noSpacing) {
        this.el.nativeElement.classList.add('company');
      } else {
        this.el.nativeElement.classList.remove('company');
      }
    }
  }

  constructor(private el: ElementRef) { }
}
