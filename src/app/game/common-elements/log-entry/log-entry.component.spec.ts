import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FontAwesomeIconStubComponent } from '../../../test-helpers/font-awesome-icon-stub-component.spec';

import { AbsPipe } from '../abs.pipe';
import { CompanyColorsDirective } from '../company-colors/company-colors.directive';
import { EVENT_TYPE, GameEvent } from '../../../models/events';
import { GameStateService } from '../../../services/game-state.service';
import { LogEntryComponent } from './log-entry.component';

describe('LogEntryComponent', () => {
  let component: LogEntryComponent;
  let fixture: ComponentFixture<LogEntryComponent>;
  let expectedGameEvent: Partial<GameEvent>;
  const gameStateServiceStub: Partial<GameStateService> = { };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        LogEntryComponent,
        FontAwesomeIconStubComponent,
        CompanyColorsDirective,
        AbsPipe
      ],
      providers: [
        { provide: GameStateService, useValue: gameStateServiceStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogEntryComponent);
    component = fixture.componentInstance;
    // Mock GameEvent
    expectedGameEvent = { type: EVENT_TYPE.SETTINGS_CHANGED, version: 0, apply: x => x };
    component.entry = expectedGameEvent;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
