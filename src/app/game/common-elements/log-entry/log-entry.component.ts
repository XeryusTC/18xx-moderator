import { Component, Input, OnInit } from '@angular/core';

import { DIVIDEND_METHOD, EVENT_TYPE } from '../../../models/events';
import { GameStateService } from '../../../services/game-state.service';
import { Company } from '../../../models/state';

@Component({
  selector: 'app-log-entry',
  templateUrl: './log-entry.component.html',
  styleUrls: ['./log-entry.component.sass']
})
export class LogEntryComponent {
  EVENT_TYPE: typeof EVENT_TYPE = EVENT_TYPE;
  DIVIDEND_METHOD: typeof DIVIDEND_METHOD = DIVIDEND_METHOD;

  @Input() entry;
  showDetails = true;

  toggleDetails() {
    this.showDetails = !this.showDetails;
  }

  constructor(public gameState: GameStateService) { }

  company(name: string): Company {
    if (this.gameState.state.allCompanies.hasOwnProperty(name)) {
      return this.gameState.state.allCompanies[name];
    } else {
      return null;
    }
  }

  revenue() {
    if (this.entry.type === EVENT_TYPE.OPERATED) {
      return this.entry.revenues.reduce((a, b) => a + b, 0);
    }
    return 0;
  }
}
