import { Injectable } from '@angular/core';

import { GameEvent } from '../../models/events';
import { GameState } from '../../models/state';
import { GameStateService } from '../../services/game-state.service';

@Injectable({
  providedIn: 'root'
})
export class EventQueueService {
  events: GameEvent[] = [];
  state: GameState = new GameState();

  constructor(private gameState: GameStateService) {
    this.gameState.updated.subscribe(() => {
      this._applyEvents();
    });
    this._applyEvents();
  }

  addEvent(event: GameEvent) {
    this.events.push(event);
    this._applyEvents();
  }

  reset() {
    this.events = [];
  }

  private _applyEvents() {
    this.state = this.gameState.state || new GameState();
    for (const e of this.events) {
      this.state = e.apply(this.state);
    }
  }
}
