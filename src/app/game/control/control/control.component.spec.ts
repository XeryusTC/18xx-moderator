import { Component } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';

import { ActivatedRouteStub } from '../../../test-helpers/activated-route-stub.spec';
import { RouterLinkDirectiveStub } from '../../../test-helpers/router-link-directive-stub.spec';
import { RouterOutletStub } from '../../../test-helpers/router-outlet-stub.spec';

import { ControlComponent } from './control.component';
import { MenuComponent } from '../../../menu/menu.component';
import { GameStateService } from '../../../services/game-state.service';

@Component({ selector: 'app-queue', template: '' })
class QueueStubComponent { }

describe('ControlComponent', () => {
  let component: ControlComponent;
  let fixture: ComponentFixture<ControlComponent>;
  const activatedRouteStub = new ActivatedRouteStub({ id: 'test-game' });
  const gameStateServiceStub: Partial<GameStateService> = {
    init: x => null
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ControlComponent,
        RouterLinkDirectiveStub,
        RouterOutletStub,
        MenuComponent,
        QueueStubComponent
      ],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: GameStateService, useValue: gameStateServiceStub}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
