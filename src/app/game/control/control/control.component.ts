import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { takeUntil } from 'rxjs/operators';

import { GameStateService } from '../../../services/game-state.service';

import { CleanComponent } from '../../../clean.component';

@Component({
  selector: 'app-control',
  templateUrl: './control.component.html',
  styleUrls: ['./control.component.sass']
})
export class ControlComponent extends CleanComponent implements OnInit {
  gameId: string;

  constructor(private route: ActivatedRoute, private gameState: GameStateService) {
    super();
  }

  ngOnInit() {
    this.route.params
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(params => {
        this.gameId = params['id'];
        if (this.gameId !== this.gameState.id) {
          this.gameState.init(this.gameId);
        }
      });
  }
}
