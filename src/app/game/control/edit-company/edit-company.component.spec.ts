import { EventEmitter } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { ColorChromeModule } from 'ngx-color/chrome';

import { ActivatedRouteStub } from '../../../test-helpers/activated-route-stub.spec';
import { FontAwesomeIconStubComponent } from '../../../test-helpers/font-awesome-icon-stub-component.spec';
import { RouterLinkDirectiveStub } from '../../../test-helpers/router-link-directive-stub.spec';

import { CompanyColorsDirective } from '../../common-elements/company-colors/company-colors.directive';
import { EditCompanyComponent } from './edit-company.component';
import { GameStateService } from '../../../services/game-state.service';

describe('EditCompanyComponent', () => {
  let component: EditCompanyComponent;
  let fixture: ComponentFixture<EditCompanyComponent>;
  const activatedRouteStub = new ActivatedRouteStub({id: 'test-game'});
  const routerSpy = jasmine.createSpyObj('Router', ['navigate']);
  const gameStateStub: Partial<GameStateService> = {
    updated: new EventEmitter<void>()
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ColorChromeModule,
        ReactiveFormsModule
      ],
      declarations: [
        CompanyColorsDirective,
        EditCompanyComponent,
        FontAwesomeIconStubComponent,
        RouterLinkDirectiveStub
      ],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: Router, useValue: routerSpy },
        { provide: GameStateService, useValue: gameStateStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
