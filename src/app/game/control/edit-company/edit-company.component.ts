import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { ColorEvent } from 'ngx-color';
import { takeUntil } from 'rxjs/operators';

import { CleanComponent } from '../../../clean.component';
import { EventQueueService } from '../event-queue.service';
import { CompanyEditedEvent, SHARE_SOURCE } from '../../../models/events';
import { GameStateService } from '../../../services/game-state.service';

@Component({
  selector: 'app-edit-company',
  templateUrl: './edit-company.component.html',
  styleUrls: ['./edit-company.component.sass']
})
export class EditCompanyComponent extends CleanComponent implements OnInit {
  SHARE_SOURCE: typeof SHARE_SOURCE = SHARE_SOURCE;
  name: string;
  editForm: FormGroup;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private fb: FormBuilder,
              private gameState: GameStateService,
              public eq: EventQueueService) {
    super();
    this.editForm = this.fb.group({
      shareCount: [null, [Validators.required, Validators.min(1)]],
      shareSource: [SHARE_SOURCE.POOL, Validators.required],
      textColor: [],
      backgroundColor: []
    });
  }

  ngOnInit() {
    this.route.params
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(params => {
        this.name = params['name'];
        if (this.eq.state.activeCompanies.hasOwnProperty(this.name)) {
          const company = this.eq.state.activeCompanies[this.name];
          this.editForm.patchValue({
            shareCount: company.shareCount,
            textColor: company.textColor,
            backgroundColor: company.backgroundColor
          });
        }
      });

    this.gameState.updated
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(() => {
        if (this.eq.state.activeCompanies.hasOwnProperty(this.name)) {
          const company = this.eq.state.activeCompanies[this.name];
          this.editForm.patchValue({
            shareCount: company.shareCount,
            textColor: company.textColor,
            backgroundColor: company.backgroundColor
          });
        }
      });
  }

  edit(event: Event) {
    event.preventDefault();
    this.eq.addEvent(new CompanyEditedEvent(
      this.name,
      this.editForm.get('shareCount').value,
      this.editForm.get('shareSource').value,
      this.editForm.get('textColor').value,
      this.editForm.get('backgroundColor').value
    ));
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  handleTextColorChange(event: ColorEvent) {
    this.editForm.patchValue({ textColor: event.color.hex });
  }

  handleBackgroundColorChange(event: ColorEvent) {
    this.editForm.patchValue({ backgroundColor: event.color.hex });
  }
}
