import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { takeUntil } from 'rxjs/operators';

import { EventStoreService } from '../../../services/event-store.service';
import { EventQueueService } from '../event-queue.service';
import { GameStateService } from '../../../services/game-state.service';

import { CleanComponent } from '../../../clean.component';

@Component({
  selector: 'app-queue',
  templateUrl: './queue.component.html',
  styleUrls: ['./queue.component.sass']
})
export class QueueComponent extends CleanComponent implements OnInit {
  gameId: string;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private es: EventStoreService,
              public eq: EventQueueService) {
    super();
  }

  ngOnInit() {
    this.route.params
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(params => {
        this.gameId = params['id'];
      });
  }

  commit() {
    console.log('Committing', this.eq.events);
    this.es.postEvents(this.gameId, this.eq.events)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(() => { });
    this.eq.reset();
  }

  reset() {
    this.eq.reset();
  }

  remove(id) {
    this.eq.events.splice(id, 1);
  }
}
