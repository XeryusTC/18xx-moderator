import { Component, Input } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';

import { ActivatedRouteStub } from '../../../test-helpers/activated-route-stub.spec';
import { FontAwesomeIconStubComponent } from '../../../test-helpers/font-awesome-icon-stub-component.spec';

import { QueueComponent } from './queue.component';
import { EventQueueService } from '../event-queue.service';
import { EventStoreService } from '../../../services/event-store.service';

@Component({ selector: 'app-log-entry', template: '' })
class LogEntryStubComponent {
  @Input() entry;
}

describe('QueueComponent', () => {
  let component: QueueComponent;
  let fixture: ComponentFixture<QueueComponent>;
  const activatedRouteStub = new ActivatedRouteStub({ id: 'test-game' });
  const routerSpy = jasmine.createSpyObj('Router', ['navigate']);
  const eventQueueServiceStub: Partial<EventQueueService> = { events: [] };
  const eventStoreServiceStub: Partial<EventStoreService> = {};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        FontAwesomeIconStubComponent,
        LogEntryStubComponent,
        QueueComponent
      ],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: Router, useValue: routerSpy },
        { provide: EventQueueService, useValue: eventQueueServiceStub },
        { provide: EventStoreService, useValue: eventStoreServiceStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QueueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
