import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { takeUntil } from 'rxjs/operators';

import { CleanComponent } from '../../../clean.component';
import { EventQueueService } from '../event-queue.service';
import { SHARE_SOURCE, ShareTransferredEvent } from '../../../models/events';
import { Company } from '../../../models/state';

@Component({
  selector: 'app-buy-share',
  templateUrl: './buy-share.component.html',
  styleUrls: ['./buy-share.component.sass']
})
export class BuyShareComponent extends CleanComponent implements OnInit {
  SHARE_SOURCE: typeof SHARE_SOURCE = SHARE_SOURCE;

  type: string;
  name: string;
  buyForm: FormGroup;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private fb: FormBuilder,
              public eq: EventQueueService) {
    super();
    this.buyForm = this.fb.group({
      amount: [1, [Validators.required, Validators.min(1)]],
      company: ['', Validators.required],
      from: ['', Validators.required],
      price: [0, Validators.required],
    });
  }

  ngOnInit() {
    this.route.params
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(params => {
        this.type = params['type'];
        this.name = params['name'];
      });

    this.buyForm.controls['company'].valueChanges
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((company: string) => {
        this.buyForm.patchValue({ price: this.eq.state.activeCompanies[company].value });
      });
  }

  buy(event: Event) {
    event.preventDefault();
    this.eq.addEvent(new ShareTransferredEvent(
      this.name,
      this.buyForm.value['from'],
      this.buyForm.value['company'],
      this.buyForm.value['amount'],
      this.buyForm.value['price']
    ));
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  playerHasShares(player: string): boolean {
    return this.eq.state.players.hasOwnProperty(player)
      && this.eq.state.players[player].shares.hasOwnProperty(this.buyForm.value['company'])
      && this.eq.state.players[player].shares[this.buyForm.value['company']] !== 0;
  }

  companyHasShares(company: string): boolean {
    return this.eq.state.activeCompanies.hasOwnProperty(company)
      && this.eq.state.activeCompanies[company].shares.hasOwnProperty(this.buyForm.value['company'])
      && this.eq.state.activeCompanies[company].shares[this.buyForm.value['company']] !== 0;
  }

  ipoSharesAvailable(company: string): boolean {
    const available = this.eq.state.activeCompanies[company].ipoShares > 0;
    if (!available && this.buyForm.value['from'] === 'IPO') {
      this.buyForm.patchValue({from: ''});
    }
    return available;
  }

  poolSharesAvailable(company: string): boolean {
    const available = this.eq.state.activeCompanies[company].poolShares > 0;
    if (!available && this.buyForm.value['from'] === 'pool') {
      this.buyForm.patchValue({from: ''});
    }
    return available;
  }

  allowedToBuy(company: Company, companyName: string): boolean {
    return this.type === 'player'
      || this.eq.state.settings.companiesCanOwnOtherCompanies
      || this.name === companyName;
  }
}
