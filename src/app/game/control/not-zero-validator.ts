import { AbstractControl } from '@angular/forms';

export function notZeroValidator(control: AbstractControl): {[key: string]: any} | null {
  return control.value === 0 ? { 'notZero': { value: control.value }} : null;
}
