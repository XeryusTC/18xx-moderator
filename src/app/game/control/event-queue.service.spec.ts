import { EventEmitter } from '@angular/core';
import { TestBed } from '@angular/core/testing';

import { EventQueueService } from './event-queue.service';
import { GameStateService } from '../../services/game-state.service';

describe('EventQueueService', () => {
  const gameStateServiceStub: Partial<GameStateService> = {
    updated: new EventEmitter<void>()
  };

  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: GameStateService, useValue: gameStateServiceStub }
    ]
  }));

  it('should be created', () => {
    const service: EventQueueService = TestBed.get(EventQueueService);
    expect(service).toBeTruthy();
  });
});
