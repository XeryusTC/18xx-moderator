import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { ColorChromeModule } from 'ngx-color/chrome';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { CommonElementsModule } from '../common-elements/common-elements.module';
import { ControlRoutingModule } from './control-routing.module';
import { MenuModule } from '../../menu/menu.module';

import { BuyShareComponent } from './buy-share/buy-share.component';
import { ControlComponent } from './control/control.component';
import { EditCompanyComponent } from './edit-company/edit-company.component';
import { OperateComponent } from './operate/operate.component';
import { QueueComponent } from './queue/queue.component';
import { SellShareComponent } from './sell-share/sell-share.component';
import { StartComponent } from './start/start.component';
import { TurnOverviewComponent } from './turn-overview/turn-overview.component';
import { TransferMoneyComponent } from './transfer-money/transfer-money.component';
import { UndoComponent } from './undo/undo.component';
import { RemoveCompanyComponent } from './remove-company/remove-company.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ColorChromeModule,
    FontAwesomeModule,

    CommonElementsModule,
    MenuModule,

    // Routing
    ControlRoutingModule
  ],
  declarations: [
    BuyShareComponent,
    ControlComponent,
    EditCompanyComponent,
    OperateComponent,
    QueueComponent,
    SellShareComponent,
    StartComponent,
    TurnOverviewComponent,
    TransferMoneyComponent,
    UndoComponent,
    RemoveCompanyComponent
  ]
})
export class ControlModule { }
