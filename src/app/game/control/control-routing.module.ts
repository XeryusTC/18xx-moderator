import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BuyShareComponent } from './buy-share/buy-share.component';
import { ControlComponent } from './control/control.component';
import { EditCompanyComponent } from './edit-company/edit-company.component';
import { RemoveCompanyComponent } from './remove-company/remove-company.component';
import { SellShareComponent } from './sell-share/sell-share.component';
import { StartComponent } from './start/start.component';
import { TransferMoneyComponent } from './transfer-money/transfer-money.component';
import { TurnOverviewComponent } from './turn-overview/turn-overview.component';
import { OperateComponent } from './operate/operate.component';
import { UndoComponent } from './undo/undo.component';

const routes: Routes = [
  {
    path: 'control/:id',
    component: ControlComponent,
    children: [
      {
        path: 'turn/:type/:name/transfer-money',
        component: TransferMoneyComponent
      },
      {
        path: 'turn/:type/:name/buy-share',
        component: BuyShareComponent
      },
      {
        path: 'turn/:type/:name/sell-share',
        component: SellShareComponent,
      },
      {
        path: 'turn/:type/:name/operate',
        component: OperateComponent
      },
      {
        path: 'turn/company/:name/edit',
        component: EditCompanyComponent
      },
      {
        path: 'turn/company/:name/remove',
        component: RemoveCompanyComponent
      },
      {
        path: 'turn/private/:name/remove',
        component: RemoveCompanyComponent
      },
      {
        path: 'turn/:type/:name',
        component: TurnOverviewComponent
      },
      {
        path: 'undo/:page',
        component: UndoComponent
      },
      {
        path: '',
        component: StartComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ControlRoutingModule { }
