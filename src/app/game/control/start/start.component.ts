import { Component, OnInit } from '@angular/core';

import { CompoundEvent, DIVIDEND_METHOD, GameEvent, OperatedEvent } from '../../../models/events';
import { EventQueueService } from '../event-queue.service';

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.sass']
})
export class StartComponent implements OnInit {

  constructor(public eq: EventQueueService) { }

  ngOnInit() {
  }

  newOR() {
    const events: GameEvent[] = [];
    // TODO: operate all privates when they are reimplemented
    this.eq.addEvent(new CompoundEvent('Start new OR', events));
  }
}
