import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouterLinkDirectiveStub } from '../../../test-helpers/router-link-directive-stub.spec';

import { CompanyColorsDirective } from '../../common-elements/company-colors/company-colors.directive';
import { EventQueueService } from '../event-queue.service';
import { GameState } from '../../../models/state';
import { StartComponent } from './start.component';

describe('StartComponent', () => {
  let component: StartComponent;
  let fixture: ComponentFixture<StartComponent>;
  const eventQueueServiceStub: Partial<EventQueueService> = { state: new GameState() };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CompanyColorsDirective,
        RouterLinkDirectiveStub,
        StartComponent
      ],
      providers: [
        { provide: EventQueueService, useValue: eventQueueServiceStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
