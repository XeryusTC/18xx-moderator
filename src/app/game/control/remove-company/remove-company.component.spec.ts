import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';

import { ActivatedRouteStub } from '../../../test-helpers/activated-route-stub.spec';
import { RouterLinkDirectiveStub } from '../../../test-helpers/router-link-directive-stub.spec';

import { CompanyColorsDirective } from '../../common-elements/company-colors/company-colors.directive';
import { EventQueueService } from '../event-queue.service';
import { FontAwesomeIconStubComponent } from '../../../test-helpers/font-awesome-icon-stub-component.spec';
import { GameState } from '../../../models/state';
import { RemoveCompanyComponent } from './remove-company.component';

describe('RemoveCompanyComponent', () => {
  let component: RemoveCompanyComponent;
  let fixture: ComponentFixture<RemoveCompanyComponent>;
  const activatedRouteStub = new ActivatedRouteStub({ id: 'test-game' });
  const routerStub = jasmine.createSpyObj('Router', ['navigate']);
  const eventQueueServiceStub: Partial<EventQueueService> = { state: new GameState() };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CompanyColorsDirective,
        FontAwesomeIconStubComponent,
        RemoveCompanyComponent,
        RouterLinkDirectiveStub
      ],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: Router, useValue: routerStub },
        { provide: EventQueueService, useValue: eventQueueServiceStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoveCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
