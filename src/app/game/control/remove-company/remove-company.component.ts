import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { takeUntil } from 'rxjs/operators';

import { CleanComponent } from '../../../clean.component';
import { CompanyRemovedEvent } from '../../../models/events';
import { EventQueueService } from '../event-queue.service';

@Component({
  selector: 'app-remove-company',
  templateUrl: './remove-company.component.html',
  styleUrls: ['./remove-company.component.sass']
})
export class RemoveCompanyComponent extends CleanComponent implements OnInit {
  name: string;

  constructor(private route: ActivatedRoute, private router: Router, public eq: EventQueueService) {
    super();
  }

  ngOnInit() {
    this.route.params
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(params => {
        this.name = params['name'];
      });
  }

  remove() {
    this.eq.addEvent(new CompanyRemovedEvent(this.name));
    this.router.navigate(['../../../..'], { relativeTo: this.route });
  }
}
