import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { takeUntil } from 'rxjs/operators';

import { CleanComponent } from '../../../clean.component';
import { EventQueueService } from '../event-queue.service';
import { SHARE_SOURCE, ShareTransferredEvent } from '../../../models/events';
import { Company } from '../../../models/state';

@Component({
  selector: 'app-sell-share',
  templateUrl: './sell-share.component.html',
  styleUrls: ['./sell-share.component.sass']
})
export class SellShareComponent extends CleanComponent implements OnInit {
  SHARE_SOURCE: typeof SHARE_SOURCE = SHARE_SOURCE;

  type: string;
  name: string;
  sellForm: FormGroup;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private fb: FormBuilder,
              public eq: EventQueueService) {
    super();
  }

  ngOnInit() {
    this.route.params
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(params => {
        this.type = params['type'];
        this.name = params['name'];
        this.sellForm = this.fb.group({
          amount: [1, [Validators.required, Validators.min(1)]],
          company: ['', Validators.required],
          to: ['pool', Validators.required],
          price: [0, Validators.required],
        });
      });

    this.sellForm.controls['company'].valueChanges
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((company: string) => {
        this.sellForm.patchValue({ price: this.eq.state.activeCompanies[company].value });
      });
  }

  sell(e: Event) {
    e.preventDefault();
    this.eq.addEvent(new ShareTransferredEvent(
      this.name,
      this.sellForm.value['to'],
      this.sellForm.value['company'],
      -this.sellForm.value['amount'],
      this.sellForm.value['price']
    ));
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  allowedToSell(companyName: string, company: Company, doTargetCheck = false): boolean {
    const selectedCompany = this.sellForm.get('company').value;
    // Check if a player can sell the share
    const playerCanSell = this.type === 'player'
      && !doTargetCheck // Players can sell anything that is legal to be sold, they can also always be sold to
      && (this.eq.state.settings.shortingAllowed // Players can always sell when shorting is allowed
        // Allow selling shares if the player owns at least one
        || (this.eq.state.players[this.name].shares.hasOwnProperty(companyName)
          && this.eq.state.players[this.name].shares[companyName] > 0));

    return playerCanSell
      || this.name === companyName // A company can always sell its own shares
      || this.eq.state.settings.companiesCanOwnOtherCompanies
      // Do not allow selling shares to companies that are not allowed to own those shares
      || (doTargetCheck
        && (selectedCompany === companyName
          || (selectedCompany)));
  }
}
