import { Component, EventEmitter, Input } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';

import { ActivatedRouteStub } from '../../../test-helpers/activated-route-stub.spec';

import { CompanyColorsDirective } from '../../common-elements/company-colors/company-colors.directive';
import { EventQueueService } from '../event-queue.service';
import { FontAwesomeIconStubComponent } from '../../../test-helpers/font-awesome-icon-stub-component.spec';
import { GameState } from '../../../models/state';
import { GameStateService } from '../../../services/game-state.service';
import { RouterLinkDirectiveStub } from '../../../test-helpers/router-link-directive-stub.spec';
import { UndoComponent } from './undo.component';

@Component({ selector: 'app-log-entry', template: '' })
class LogEntryStubComponent {
  @Input() entry;
}

describe('UndoComponent', () => {
  let component: UndoComponent;
  let fixture: ComponentFixture<UndoComponent>;
  const activatedRouteStub = new ActivatedRouteStub({ id: 'test-game' });
  const routerSpy = jasmine.createSpyObj('Router', ['navigate']);
  const eventQueueServiceStub: Partial<EventQueueService> = { state: new GameState() };
  const gameStateServiceStub: Partial<GameStateService> = {
    log: [],
    updated: new EventEmitter<void>()
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CompanyColorsDirective,
        FontAwesomeIconStubComponent,
        LogEntryStubComponent,
        RouterLinkDirectiveStub,
        UndoComponent
      ],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: Router, useValue: routerSpy },
        { provide: GameStateService, useValue: gameStateServiceStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UndoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
