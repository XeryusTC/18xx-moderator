import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { takeUntil } from 'rxjs/operators';

import { CleanComponent } from '../../../clean.component';
import { EventQueueService } from '../event-queue.service';
import { GameStateService } from '../../../services/game-state.service';
import { EVENT_TYPE, GameEvent, UndoneEvent } from '../../../models/events';

@Component({
  selector: 'app-undo',
  templateUrl: './undo.component.html',
  styleUrls: ['./undo.component.sass']
})
export class UndoComponent extends CleanComponent implements OnInit {
  EVENTS_PER_PAGE = 20;

  page: number;
  numPages: number;

  get logSlice(): GameEvent[] {
    return this.gameState.log.slice(
      Math.max(0, this.gameState.log.length - (this.page + 1) * this.EVENTS_PER_PAGE),
      this.gameState.log.length - this.page * this.EVENTS_PER_PAGE
    ).reverse();
  }

  constructor(private route: ActivatedRoute,
              private router: Router,
              public gameState: GameStateService,
              private eq: EventQueueService) {
    super();
  }

  ngOnInit() {
    this.route.params
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(params => {
        this.page = +params['page'];
        this.numPages = Math.floor((this.gameState.log.length - 1) / this.EVENTS_PER_PAGE);
      });

    this.gameState.updated
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(() => {
        this.numPages = Math.floor((this.gameState.log.length - 1) / this.EVENTS_PER_PAGE);
      });
  }

  undo(id: number) {
    this.eq.addEvent(new UndoneEvent(id));
  }

  isUndoable(event: GameEvent): boolean {
    // Check if there is an undone event pointing to this. This is used to prevent having to undone events pointing to
    // the same event, creating a hard-to-undo tree
    const hasAssociatedUndoneEvent: boolean = this.gameState.log
      .filter(e => e.type === EVENT_TYPE.UNDONE)
      .some((e: UndoneEvent) => e.eventNumber === event.number);
    return event.number !== 0 // Cannot undo the very first event
      && !event.undone && !hasAssociatedUndoneEvent // Cannot undo events that already have been undone
      && event.type !== EVENT_TYPE.COMPANY_ADDED
      && event.type !== EVENT_TYPE.PLAYER_ADDED;
  }
}
