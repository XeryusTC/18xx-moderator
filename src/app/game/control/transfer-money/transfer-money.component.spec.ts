import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { ActivatedRouteStub } from '../../../test-helpers/activated-route-stub.spec';

import { CompanyColorsDirective } from '../../common-elements/company-colors/company-colors.directive';
import { EventQueueService } from '../event-queue.service';
import { GameState } from '../../../models/state';
import { TransferMoneyComponent } from './transfer-money.component';
import { FontAwesomeIconStubComponent } from '../../../test-helpers/font-awesome-icon-stub-component.spec';
import { RouterLinkDirectiveStub } from '../../../test-helpers/router-link-directive-stub.spec';

describe('TransferMoneyComponent', () => {
  let component: TransferMoneyComponent;
  let fixture: ComponentFixture<TransferMoneyComponent>;
  const activatedRouteStub = new ActivatedRouteStub({ id: 'test-game' });
  const routerSpy = jasmine.createSpyObj('Router', ['navigate']);
  const eventQueueServiceStub: Partial<EventQueueService> = { state: new GameState() };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule
      ],
      declarations: [
        CompanyColorsDirective,
        FontAwesomeIconStubComponent,
        RouterLinkDirectiveStub,
        TransferMoneyComponent
      ],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: Router, useValue: routerSpy },
        { provide: EventQueueService, useValue: eventQueueServiceStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferMoneyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
