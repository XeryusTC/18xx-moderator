import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { takeUntil } from 'rxjs/operators';

import { EventQueueService } from '../event-queue.service';

import { CleanComponent } from '../../../clean.component';
import { MoneyTransferredEvent } from '../../../models/events';
import { notZeroValidator } from '../not-zero-validator';

@Component({
  selector: 'app-transfer-money',
  templateUrl: './transfer-money.component.html',
  styleUrls: ['./transfer-money.component.sass']
})
export class TransferMoneyComponent extends CleanComponent implements OnInit {
  type: string;
  name: string;
  transferForm: FormGroup;
  reasons = [
    'other', // Other is special, it should always be first
    'loan',
    'track lay',
    'train',
    'token'
  ];

  constructor(private route: ActivatedRoute,
              private router: Router,
              private fb: FormBuilder,
              public eq: EventQueueService) {
    super();
  }

  ngOnInit() {
    this.route.params
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(params => {
        this.type = params['type'];
        this.name = params['name'];

        this.transferForm = this.fb.group({
          amount: [null, [Validators.required, notZeroValidator]],
          target: ['bank', Validators.required],
          reason: [this.reasons[0]]
        });
      });
  }

  transfer(e) {
    e.preventDefault();
    this.eq.addEvent(new MoneyTransferredEvent(
      this.name,
      this.transferForm.value['target'],
      this.transferForm.value['amount'],
      this.transferForm.value['reason'] === this.reasons[0] ? '' : this.transferForm.value['reason']
    ));
    this.router.navigate(['../'], { relativeTo: this.route });
  }
}
