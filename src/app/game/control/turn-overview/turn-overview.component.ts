import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { takeUntil } from 'rxjs/operators';

import { CleanComponent } from '../../../clean.component';
import { EventQueueService } from '../event-queue.service';

@Component({
  selector: 'app-turn-overview',
  templateUrl: './turn-overview.component.html',
  styleUrls: ['./turn-overview.component.sass']
})
export class TurnOverviewComponent extends CleanComponent implements OnInit {
  type: string;
  name: string;

  constructor(private route: ActivatedRoute, public eq: EventQueueService) {
    super();
  }

  ngOnInit() {
    this.route.params
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(params => {
        this.type = params['type'];
        this.name = params['name'];
      });
  }
}
