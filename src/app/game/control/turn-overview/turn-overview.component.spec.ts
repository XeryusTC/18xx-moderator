import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';

import { ActivatedRouteStub } from '../../../test-helpers/activated-route-stub.spec';

import { CompanyColorsDirective } from '../../common-elements/company-colors/company-colors.directive';
import { FontAwesomeIconStubComponent } from '../../../test-helpers/font-awesome-icon-stub-component.spec';
import { RouterLinkDirectiveStub } from '../../../test-helpers/router-link-directive-stub.spec';
import { TurnOverviewComponent } from './turn-overview.component';
import { EventQueueService } from '../event-queue.service';

describe('TurnOverviewComponent', () => {
  let component: TurnOverviewComponent;
  let fixture: ComponentFixture<TurnOverviewComponent>;
  const activatedRouteStub = new ActivatedRouteStub({ id: 'test-game' });
  const eventQueueServiceStub: Partial<EventQueueService> = {};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CompanyColorsDirective,
        FontAwesomeIconStubComponent,
        RouterLinkDirectiveStub,
        TurnOverviewComponent
      ],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: EventQueueService, useValue: eventQueueServiceStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TurnOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
