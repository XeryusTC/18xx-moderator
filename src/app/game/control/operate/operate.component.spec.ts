import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { ActivatedRouteStub } from '../../../test-helpers/activated-route-stub.spec';

import { CompanyColorsDirective } from '../../common-elements/company-colors/company-colors.directive';
import { EventQueueService } from '../event-queue.service';
import { GameState } from '../../../models/state';
import { OperateComponent } from './operate.component';
import { RouterLinkDirectiveStub } from '../../../test-helpers/router-link-directive-stub.spec';
import { FontAwesomeIconStubComponent } from '../../../test-helpers/font-awesome-icon-stub-component.spec';
import { CompanyAddedEvent, SettingsChangedEvent, SHARE_SOURCE } from '../../../models/events';

describe('OperateComponent', () => {
  let component: OperateComponent;
  let fixture: ComponentFixture<OperateComponent>;
  const activatedRouteStub = new ActivatedRouteStub({ id: 'test-game', name: 'company' });
  const routerSpy = jasmine.createSpyObj('Router', ['navigate']);
  const eventQueueServiceStub: Partial<EventQueueService> = {
    state: [
      new SettingsChangedEvent(1000, false, false, false, '$', SHARE_SOURCE.IPO, false, true, true, true),
      new CompanyAddedEvent('company', 0, 10, 'white', 'black', SHARE_SOURCE.IPO)
    ].reduce((s, event) => event.apply(s), new GameState())
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule
      ],
      declarations: [
        OperateComponent,
        CompanyColorsDirective,
        FontAwesomeIconStubComponent,
        RouterLinkDirectiveStub
      ],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: Router, useValue: routerSpy },
        { provide: EventQueueService, useValue: eventQueueServiceStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
