import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';

import { takeUntil } from 'rxjs/operators';

import { EventQueueService } from '../event-queue.service';
import { CleanComponent } from '../../../clean.component';
import { notZeroValidator } from '../not-zero-validator';
import { DIVIDEND_METHOD, OperatedEvent } from '../../../models/events';

@Component({
  selector: 'app-operate',
  templateUrl: './operate.component.html',
  styleUrls: ['./operate.component.sass']
})
export class OperateComponent extends CleanComponent implements OnInit {
  DIVIDEND_METHOD: typeof DIVIDEND_METHOD = DIVIDEND_METHOD;

  name: string;
  operateForm: FormGroup;

  get revenues() {
    return this.operateForm.get('revenues') as FormArray;
  }

  constructor(private route: ActivatedRoute,
              private router: Router,
              private fb: FormBuilder,
              public eq: EventQueueService) {
    super();
  }

  ngOnInit() {
    this.route.params
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(params => {
        this.name = params['name'];
        this.operateForm = this.fb.group({
          revenues: this.fb.array(
            this.eq.state.activeCompanies[this.name].revenues
              .map(v => this.fb.control(v ))
              .concat([this.fb.control(null)])
          )
        });
      });
  }

  operate(method: DIVIDEND_METHOD) {
    this.eq.addEvent(new OperatedEvent(
      this.name,
      this.revenues.controls.map(c => c.value).filter(v => v !== null),
      method
    ));
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  addRevenueField() {
    this.revenues.push(this.fb.control(null));
  }

  removeRevenueField(index: number) {
    this.revenues.removeAt(index);
  }

  totalRevenue() {
    return this.revenues.controls
      .map(c => c.value)
      .filter(v => v !== null)
      .reduce((a, b) => a + b, 0);
  }
}
