import { Component, OnInit } from '@angular/core';
import { GameStateService } from '../../../services/game-state.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { CleanComponent } from '../../../clean.component';

@Component({
  selector: 'app-net-worth',
  templateUrl: './net-worth.component.html',
  styleUrls: ['./net-worth.component.sass']
})
export class NetWorthComponent extends CleanComponent implements OnInit {
  netWorthForm: FormGroup;
  netWorth = {};
  subNetWorth = {};  // Keeps track of the net worth of a single company for each player

  constructor(private fb: FormBuilder, public gameState: GameStateService) {
    super();
  }

  ngOnInit() {
    this.gameState.updated
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(() => this.createForm());
    if (this.gameState.state) {
      this.createForm();
    }
  }

  createForm() {
    const group = {};
    for (const company in this.gameState.state.activeCompanies) {
      if (this.gameState.state.activeCompanies.hasOwnProperty(company)) {
        group[company] = this.fb.control(this.gameState.state.activeCompanies[company].value);
      }
    }
    this.netWorthForm = this.fb.group(group);
    for (const company in this.gameState.state.activeCompanies) {
      if (this.gameState.state.activeCompanies.hasOwnProperty(company)) {
        this.netWorthForm.controls[company].valueChanges
          .pipe(takeUntil(this.ngUnsubscribe))
          .subscribe(() => this.calculateNetWorth());
      }
    }
    this.calculateNetWorth();
  }

  calculateNetWorth() {
    const players = this.gameState.state.players;
    const companies = this.gameState.state.activeCompanies;
    for (const player in this.gameState.state.players) {
      if (players.hasOwnProperty(player)) {
        this.subNetWorth[player] = {};
        this.netWorth[player] = players[player].cash;
        for (const company in companies) {
          if (companies.hasOwnProperty(company)
              && players[player].shares.hasOwnProperty(company)
              && players[player].shares[company] !== 0) {
            this.subNetWorth[player][company] = players[player].shares[company] * this.netWorthForm.value[company];
            this.netWorth[player] += this.subNetWorth[player][company];
          }
        }
      }
    }
  }
}
