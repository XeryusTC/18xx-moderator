import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NetWorthComponent } from './net-worth.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CompanyColorsDirective } from '../../common-elements/company-colors/company-colors.directive';
import { GameStateService } from '../../../services/game-state.service';
import { EventEmitter } from '@angular/core';

describe('NetWorthComponent', () => {
  let component: NetWorthComponent;
  let fixture: ComponentFixture<NetWorthComponent>;
  const gameStateServiceStub: Partial<GameStateService> = {
    log: [],
    updated: new EventEmitter<void>()
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule
      ],
      declarations: [
        CompanyColorsDirective,
        NetWorthComponent
      ],
      providers: [
        { provide: GameStateService, useValue: gameStateServiceStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NetWorthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
