import { Component } from '@angular/core';

import { GameStateService } from '../../../services/game-state.service';

@Component({
  selector: 'app-state',
  templateUrl: './state.component.html',
  styleUrls: ['./state.component.sass']
})
export class StateComponent {
  constructor(public gameState: GameStateService) { }

  privatesAvailable(): boolean {
    for (const company in this.gameState.state.activeCompanies) {
      if (this.gameState.state.activeCompanies.hasOwnProperty(company)
        && this.gameState.state.activeCompanies[company].poolShares !== 0) {
        return true;
      }
    }
    return false;
  }
}
