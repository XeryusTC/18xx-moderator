import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyColorsDirective } from '../../common-elements/company-colors/company-colors.directive';
import { GameState } from '../../../models/state';
import { GameStateService } from '../../../services/game-state.service';
import { StateComponent } from './state.component';

describe('StateComponent', () => {
  let component: StateComponent;
  let fixture: ComponentFixture<StateComponent>;
  const gameStateServiceStub: Partial<GameStateService> = {
    state: new GameState()
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CompanyColorsDirective,
        StateComponent
      ],
      providers: [
        { provide: GameStateService, useValue: gameStateServiceStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
