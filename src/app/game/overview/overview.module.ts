import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { ColorChromeModule } from 'ngx-color/chrome';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgxKjuaModule } from 'ngx-kjua';

import { CommonElementsModule } from '../common-elements/common-elements.module';
import { MenuModule } from '../../menu/menu.module';
import { OverviewRoutingModule } from './overview-routing.module';

import { AddCompanyComponent } from './add-company/add-company.component';
import { OverviewComponent } from './overview/overview.component';
import { StateComponent } from './state/state.component';
import { NetWorthComponent } from './net-worth/net-worth.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,

    ColorChromeModule,
    FontAwesomeModule,
    NgxKjuaModule,

    CommonElementsModule,
    MenuModule,

    // Routing
    OverviewRoutingModule
  ],
  declarations: [
    AddCompanyComponent,
    OverviewComponent,
    StateComponent,
    NetWorthComponent
  ],
  exports: [
    AddCompanyComponent,
    OverviewComponent
  ]
})
export class OverviewModule { }
