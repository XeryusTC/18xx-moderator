import { Component, EventEmitter, Input } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { ActivatedRouteStub } from '../../../test-helpers/activated-route-stub.spec';

import { AddCompanyComponent } from './add-company.component';
import { CompanyColorsDirective } from '../../common-elements/company-colors/company-colors.directive';
import { GameStateService } from '../../../services/game-state.service';
import { EventStoreService } from '../../../services/event-store.service';
import { GameState } from '../../../models/state';

// tslint:disable-next-line
@Component({ selector: 'color-chrome', template: '' })
class ColorChromeStubComponent {
  @Input() color;
  @Input() disableAlpha;
}

describe('AddCompanyComponent', () => {
  let component: AddCompanyComponent;
  let fixture: ComponentFixture<AddCompanyComponent>;
  const activatedRouteStub = new ActivatedRouteStub({ id: 'test-game' });
  const routerSpy = jasmine.createSpyObj('Router', ['navigate']);
  const eventStoreServiceStub: Partial<EventStoreService> = {};
  const gameStateServiceStub: Partial<GameStateService> = {
    state: new GameState(),
    updated: new EventEmitter<void>()
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule
      ],
      declarations: [
        AddCompanyComponent,
        ColorChromeStubComponent,
        CompanyColorsDirective
      ],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: Router, useValue: routerSpy },
        { provide: EventStoreService, useValue: eventStoreServiceStub },
        { provide: GameStateService, useValue: gameStateServiceStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
