import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { ColorEvent } from 'ngx-color';
import { takeUntil } from 'rxjs/operators';

import { CompanyAddedEvent, SHARE_SOURCE } from '../../../models/events';
import { EventStoreService } from '../../../services/event-store.service';
import { GameStateService } from '../../../services/game-state.service';
import { CleanComponent } from '../../../clean.component';

@Component({
  selector: 'app-add-company',
  templateUrl: './add-company.component.html',
  styleUrls: ['./add-company.component.sass']
})
export class AddCompanyComponent extends CleanComponent implements OnInit {
  gameId: string;
  newCompanyForm: FormGroup;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private es: EventStoreService,
              private gameState: GameStateService,
              private fb: FormBuilder) {
    super();
  }

  ngOnInit() {
    this.newCompanyForm = this.fb.group({
      name: ['', this.nameUnique],
      privateIncome: [0],
      shares: [10],
      startingCash: [0],
      ownsOwnShares: [this.getShareSourceSetting()],
      textColor: ['#000000'],
      backgroundColor: ['#ffffff']
    });

    this.route.parent.params
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(params => {
        this.gameId = params['id'];
      });

    this.gameState.updated
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(() => this.newCompanyForm.patchValue({ ownsOwnShares: this.getShareSourceSetting() }));
  }

  createCompany(e) {
    e.preventDefault();
    let companyEvent;
      // Determine where to put the shares of the company
    let location;
    if (this.newCompanyForm.get('ownsOwnShares').value) {
      location = SHARE_SOURCE.TREASURY;
    } else if (this.gameState.state.settings.disableIPO) {
      location = SHARE_SOURCE.POOL;
    } else {
      location = SHARE_SOURCE.IPO;
    }

    companyEvent = new CompanyAddedEvent(
      this.newCompanyForm.get('name').value.trim(),
      this.newCompanyForm.get('startingCash').value,
      this.newCompanyForm.get('shares').value,
      this.newCompanyForm.get('textColor').value,
      this.newCompanyForm.get('backgroundColor').value,
      location
    );
    this.es.postEvents(this.gameId, [companyEvent])
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(() => {
        this.router.navigate(['overview', this.gameId]);
      });
  }

  handleTextColorChange(e: ColorEvent) {
    this.newCompanyForm.patchValue({ textColor: e.color.hex });
  }

  handleBackgroundColorChange(e: ColorEvent) {
    this.newCompanyForm.patchValue({ backgroundColor: e.color.hex });
  }

  nameUnique: ValidatorFn = (control: FormControl): {[key: string]: any} | null => {
    return this.gameState.state.allCompanies.hasOwnProperty(control.value) ? { duplicateName: true } : null;
  }

  private getShareSourceSetting(): boolean {
    if (this.gameState.state.settings) {
      return this.gameState.state.settings.defaultShareSource === SHARE_SOURCE.TREASURY;
    }
    return false;
  }
}
