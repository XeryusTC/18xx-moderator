import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AddCompanyComponent } from './add-company/add-company.component';
import { OverviewComponent } from './overview/overview.component';
import { NetWorthComponent } from './net-worth/net-worth.component';
import { StateComponent } from './state/state.component';

const routes: Routes = [
  { path: 'overview/:id',
    component: OverviewComponent,
    children: [
      {
        path: 'add-company',
        component: AddCompanyComponent
      },
      { path: 'net-worth',
        component: NetWorthComponent
      },
      {
        path: '',
        component: StateComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class OverviewRoutingModule { }
