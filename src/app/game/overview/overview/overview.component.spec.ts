import { Component, Input } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';

import { RouterLinkDirectiveStub } from '../../../test-helpers/router-link-directive-stub.spec';
import { RouterOutletStub } from '../../../test-helpers/router-outlet-stub.spec';
import { ActivatedRouteStub } from '../../../test-helpers/activated-route-stub.spec';

import { GameState } from '../../../models/state';
import { GameStateService } from '../../../services/game-state.service';
import { MenuComponent } from '../../../menu/menu.component';
import { OverviewComponent } from './overview.component';

@Component({ selector: 'app-log-entry', template: '' })
class LogEntryStubComponent {
  @Input() entry;
}

@Component({ selector: 'ngx-kjua', template: '' })
class KjuaQRCodeStubComponent {
  @Input() text;
}

describe('OverviewComponent', () => {
  let component: OverviewComponent;
  let fixture: ComponentFixture<OverviewComponent>;
  const activatedRouteStub = new ActivatedRouteStub({ id: 'test-game' });
  const gameStateServiceStub: Partial<GameStateService> = {
    init: x => null,
    log: [],
    state: new GameState()
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        KjuaQRCodeStubComponent,
        LogEntryStubComponent,
        MenuComponent,
        OverviewComponent,
        RouterLinkDirectiveStub,
        RouterOutletStub
      ],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: GameStateService, useValue: gameStateServiceStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
