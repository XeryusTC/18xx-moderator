import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { takeUntil } from 'rxjs/operators';

import { GameStateService } from '../../../services/game-state.service';
import { CleanComponent } from '../../../clean.component';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.sass']
})
export class OverviewComponent extends CleanComponent implements OnInit {
  window = window;

  gameId;
  showQR = false;

  constructor(private route: ActivatedRoute, public gameState: GameStateService) {
    super();
  }

  ngOnInit() {
    this.route.params
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(params => {
        this.gameId = params['id'];
        if (this.gameId !== this.gameState.id) {
          this.gameState.init(this.gameId);
        }
      });
  }

  toggleQR() {
    this.showQR = !this.showQR;
  }
}
