import { TestBed } from '@angular/core/testing';

import { EventStoreService } from './event-store.service';
import { GameStateService } from './game-state.service';

describe('GameStateService', () => {
  const eventStoreServiceStub: Partial<EventStoreService> = {};

  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: EventStoreService, useValue: eventStoreServiceStub }
    ]
  }));

  it('should be created', () => {
    const service: GameStateService = TestBed.get(GameStateService);
    expect(service).toBeTruthy();
  });
});
