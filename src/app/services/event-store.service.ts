import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { concat, defer, from } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';

import * as uuid from 'uuid';

import { convertEventTypeToEventClass, GameEvent } from '../models/events';

const getHeaders = {
  headers: new HttpHeaders({
    'Accept': 'application/vnd.eventstore.atom+json'
  })
};

const postHeaders = {
  headers: new HttpHeaders({
    'Content-Type': 'application/vnd.eventstore.events+json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class EventStoreService {
  private continueUri: string;
  constructor(private http: HttpClient) { }

  generateId(len = 8): string {
    const s = 'abcdefghijklmnopqrstuvwxyz0123456789';
    return Array(10).join().split(',').map(() => s.charAt(Math.floor(Math.random() * s.length))).join('');
  }

  registerGame(streamId: string) {
    console.log('registering games');
    return this.http.post(
      this._streamUrl('games'),
      [{ eventId: uuid.v4(), eventType: 'NewGame', data: { id: streamId } }],
      postHeaders
    );
  }

  postEvents(streamId: string, events: GameEvent[]) {
    const eventStream = events.map((e) => {
      return {
        eventId: uuid.v4(),
        eventType: e.type,
        data: e,
      };
    });
    console.log('Posting events', eventStream);
    return this.http.post(this._streamUrl(streamId), eventStream, postHeaders);
  }

  replay(streamId: string) {
    console.log('Obtaining from start:', streamId);
    // Get the URI to the start of the stream
    return this.http.get(this._streamUrl(streamId), getHeaders)
      .pipe(mergeMap(data => {
        for (const link of data['links']) {
          if (link['relation'] === 'last') {
            return this.http.get(this._embedBodyUrl(link['uri']), getHeaders)
              .pipe(mergeMap(page => this._processPage(page)));
          }
        }
        // If this point is reached then there was just one page of events
        return this.http.get(this._embedBodyUrl(this._streamUrl(streamId)), getHeaders)
          .pipe(mergeMap(page => this._processPage(page)));
      }));
  }

  longPoll(streamId: string) {
    const longPollHeaders = { headers: getHeaders.headers.set('ES-LongPoll', '30') };
    return this.http.get(this._embedBodyUrl(this.continueUri), longPollHeaders)
      .pipe(mergeMap(page => this._processPage(page)));
  }

  private _processPage(page) {
    console.log('Processing page', page);
    /* Combine three possible streams */
    return concat(
      // First stream: the events on this page
      from(page['entries'].reverse()).pipe(map(this._convertStreamEventToGameEvent)),
      // Pick one of the two remaining streams
      defer(() => {
        if (!page['headOfStream']) {
          // Second stream: the previous page, recursive call to add those to the observable
          for (const link of page['links']) {
            if (link['relation'] === 'previous') {
              return this.http.get(this._embedBodyUrl(link['uri']), getHeaders)
                .pipe(mergeMap(previous_page => this._processPage(previous_page)));
            }
          }
        } else {
          // Third stream: the end of the stream. Add an event that holds the ETag to limit future requests
          for (const link of page['links']) {
            if (link['relation'] === 'previous') {
              this.continueUri = link['uri'];
            }
          }
        }
      })
    );
  }

  private _streamUrl(streamId: string) {
    if (window.location.hostname === 'localhost') {
      return `//localhost:2113/streams/${streamId}`;
    } else {
      return `//eventstore.18xx.nl/streams/${streamId}`;
    }
  }

  private _embedBodyUrl(origUrl: string) {
    return origUrl + '?embed=body';
  }

  private _convertStreamEventToGameEvent(event: object) {
    let type;
    try {
      type = convertEventTypeToEventClass(event['eventType']);
    } catch (e) {
      console.error('Failed to convert', event['eventType'], event['data']);
    }
    const e = type.fromEventStore(JSON.parse(event['data']));
    e.number = event['eventNumber'];
    e.created = event['updated'];
    return e;
  }
}
