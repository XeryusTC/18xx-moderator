import { EventEmitter, Injectable } from '@angular/core';

import { timer } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { EVENT_TYPE, GameEvent, UndoneEvent } from '../models/events';
import { GameState } from '../models/state';

import { EventStoreService } from './event-store.service';

import { CleanComponent } from '../clean.component';

@Injectable({
  providedIn: 'root'
})
export class GameStateService extends CleanComponent {
  id: string;
  log: GameEvent[];
  state: GameState;
  updated = new EventEmitter<void>();
  private poll = new EventEmitter<void>();

  get currencySymbol(): string {
    if (this.state.settings) {
      return this.state.settings.currencySymbol;
    } else {
      return '$';
    }
  }

  constructor(private es: EventStoreService) {
    super();
    this.poll
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(() => {
        this.es.longPoll(this.id)
          .pipe(takeUntil(this.ngUnsubscribe))
          .subscribe(this.appendEvent,
            (error) => console.error('Error during longpolling', error),
            // After we finish processing the result of the long poll, we wait 5 seconds and start the process again
            () => setTimeout(() => this.poll.emit(), 1000)
          );
        }
      );
  }

  init(streamId: string) {
    this.id = streamId;
    this.log = [];
    this.state = new GameState();

    // Replay events from the event store
    this.es.replay(this.id)
      .subscribe(this.appendEvent);
    // Start long polling, give Angular some time so that this.id is actually set before polling starts
    setTimeout(() => this.poll.emit(), 1000);
  }

  appendEvent = (event: GameEvent) => {
    this.log.push(event);
    if (event.type === EVENT_TYPE.UNDONE) {
      this._undoEvent((event as UndoneEvent).eventNumber);

      // Whenever something is undone we recalculate the state from the start
      this.state = this.log
        .filter((e: GameEvent) => !e.undone)
        .reduce((s: GameState, e: GameEvent) => e.apply(s), new GameState());
    } else {
      this.state = event.apply(this.state);
    }
    this.updated.emit();
  }

  findEvent(target: number) {
    return this.log[this._bisectLog(target)];
  }

  private _undoEvent(target: number) {
    const event = this.findEvent(target);
    event.undone = !event.undone;

    if (event.type === EVENT_TYPE.UNDONE) {
      this._undoEvent((event as UndoneEvent).eventNumber);
    }
  }

  private _bisectLog(target: number) {
    let left = 0;
    let right = this.log.length - 1;
    let cursor;
    do {
      cursor = Math.floor((right - left) / 2) + left;
      if (this.log[cursor].number < target) {
        left = cursor + 1;
      } else {
        right = cursor;
      }
    } while (this.log[cursor].number !== target);
    return cursor;
  }
}
