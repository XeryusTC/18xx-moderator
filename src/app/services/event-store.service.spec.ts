import { TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';

import { EventStoreService } from './event-store.service';

describe('EventStoreService', () => {
  const httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post']);

  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: HttpClient, useValue: httpClientSpy }
    ]
  }));

  it('should be created', () => {
    const service: EventStoreService = TestBed.get(EventStoreService);
    expect(service).toBeTruthy();
  });
});
