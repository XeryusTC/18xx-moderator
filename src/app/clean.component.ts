import { OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';

export abstract class CleanComponent implements OnDestroy {
  protected ngUnsubscribe: Subject<void> = new Subject<void>();

  public ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
