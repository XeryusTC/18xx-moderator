import { CompanyAddedEvent, SHARE_SOURCE } from '../models/events';

export interface Preset {
  bank: number;
  currencySymbol: string;
  disableIPO: boolean;
  ipoDividendToTreasury: boolean;
  poolDividendToTreasury: boolean;
  treasuryShareDividendToTreasury: boolean;
  defaultShareSource: SHARE_SOURCE;
  companiesCanOwnOtherCompanies: boolean;
  shortingAllowed: boolean;
  minimumPlayerCount: number;
  canSplitRevenue: boolean;

  privates: PrivateDescription[];
  companies: CompanyDescription[];

  calculateStartingCash(playerCount: number): number;
}

export class CompanyDescription {
  constructor(
    public name: string,
    public textColor: string,
    public backgroundColor: string,
    public shares: number,
    public startingCash: number = 0,
    public shareSource: SHARE_SOURCE = null
  ) { }
}

export class PrivateDescription {
  constructor(
    public name: string,
    public revenue: number,
  ) { }
}

export const PRESETS: { [key: string]: Preset } = {
  'None': {
    bank: 12000,
    currencySymbol: '$',
    disableIPO: false,
    ipoDividendToTreasury: false,
    poolDividendToTreasury: true,
    treasuryShareDividendToTreasury: true,
    defaultShareSource: SHARE_SOURCE.IPO,
    companiesCanOwnOtherCompanies: false,
    shortingAllowed: false,
    minimumPlayerCount: 2,
    canSplitRevenue: true,
    privates: [],
    companies: [],
    calculateStartingCash: (n) => 0
  },
  '1822': {
    bank: 12000,
    currencySymbol: '£',
    disableIPO: true,
    ipoDividendToTreasury: false,
    poolDividendToTreasury: false,
    treasuryShareDividendToTreasury: true,
    defaultShareSource: SHARE_SOURCE.TREASURY,
    companiesCanOwnOtherCompanies: false,
    shortingAllowed: false,
    canSplitRevenue: true,
    minimumPlayerCount: 3,

    privates: [],
    companies: [
      new CompanyDescription('LNWR', '#fff', '#000', 10)
    ],

    calculateStartingCash: (playerCount: number) => {
      return 2100 / playerCount;
    }
  },
  '1830': {
    bank: 12000,
    currencySymbol: '$',
    disableIPO: false,
    ipoDividendToTreasury: false,
    poolDividendToTreasury: true,
    treasuryShareDividendToTreasury: true,
    defaultShareSource: SHARE_SOURCE.IPO,
    companiesCanOwnOtherCompanies: false,
    shortingAllowed: false,
    canSplitRevenue: false,
    minimumPlayerCount: 2,

    privates: [
      new PrivateDescription('SV', 5),
      new PrivateDescription('C&StL', 10),
      new PrivateDescription('D&H', 15),
      new PrivateDescription('M&H', 20),
      new PrivateDescription('C&A', 25),
      new PrivateDescription('B&O (P)', 30),
    ],
    companies: [
      new CompanyDescription('B&M', '#fff', '#900', 10),
      new CompanyDescription('B&O', '#ff0', '#00f', 10),
      new CompanyDescription('C&O', '#000', '#0ff', 10),
      new CompanyDescription('CPR', '#fff', '#f00', 10),
      new CompanyDescription('Erie', '#000', '#ff0', 10),
      new CompanyDescription('NNH', '#000', '#f90', 10),
      new CompanyDescription('NYC', '#fff', '#000', 10),
      new CompanyDescription('PRR', '#000', '#0f0', 10),
    ],

    calculateStartingCash: (playerCount: number) => {
      return 2400 / playerCount;
    }
  },
  '1846': {
    bank: 9000,
    currencySymbol: '$',
    disableIPO: true,
    ipoDividendToTreasury: false,
    poolDividendToTreasury: false,
    treasuryShareDividendToTreasury: true,
    defaultShareSource: SHARE_SOURCE.TREASURY,
    companiesCanOwnOtherCompanies: false,
    shortingAllowed: false,
    canSplitRevenue: true,
    minimumPlayerCount: 2,


    privates: [
      new PrivateDescription('LSL', 15),
      new PrivateDescription('MC', 15),
      new PrivateDescription('O&I', 15),
      new PrivateDescription('Steamboat', 10),
      new PrivateDescription('MPC', 15),
      new PrivateDescription('TBC', 20),
      new PrivateDescription('C&WI', 10),
      new PrivateDescription('Mail', 0),
    ],
    companies: [
      new CompanyDescription('Big4', '#000', '#aaa', 1, 40, SHARE_SOURCE.POOL),
      new CompanyDescription('MS', '#000', '#aaa', 1, 60, SHARE_SOURCE.POOL),
      new CompanyDescription('B&O', '#ff0', '#00f', 10),
      new CompanyDescription('C&O', '#000', '#0ff', 10),
      new CompanyDescription('Erie', '#000', '#ff0', 10),
      new CompanyDescription('IC', '#fff', '#090', 10),
      new CompanyDescription('GT', '#000', '#f90', 10),
      new CompanyDescription('NYC', '#fff', '#000', 10),
      new CompanyDescription('PRR', '#000', '#f00', 10),
    ],

    calculateStartingCash: (playerCount: number) => {
      return 400;
    }
  }
};
