import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';
import { Router } from '@angular/router';

import { takeUntil } from 'rxjs/operators';

import { EventStoreService } from '../services/event-store.service';
import { CompanyAddedEvent, GameEvent, PlayerAddedEvent, SettingsChangedEvent, SHARE_SOURCE } from '../models/events';

import { CleanComponent } from '../clean.component';

import { CompanyDescription, PRESETS, PrivateDescription } from './PRESETS';

const MIN_PLAYERS = 2;

const ipoDisabledValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
  const disableIPO = control.get('disableIPO');
  const defaultShareSource = control.get('defaultShareSource');

  if (disableIPO.value && defaultShareSource.value === SHARE_SOURCE.IPO) {
    return { defaultShareSourceInvalid: true };
  }
  return null;
};

@Component({
  selector: 'app-new-game',
  templateUrl: './new-game.component.html',
  styleUrls: ['./new-game.component.sass']
})
export class NewGameComponent extends CleanComponent implements OnInit {
  SHARE_SOURCE: typeof SHARE_SOURCE = SHARE_SOURCE;
  PRESETS = PRESETS;

  companies: CompanyDescription[] = [];
  privates: PrivateDescription[] = [];
  newGameForm;
  selectedPreset;

  get players() {
    return this.newGameForm.get('players') as FormArray;
  }

  constructor(private fb: FormBuilder, private es: EventStoreService, private router: Router) {
    super();
  }

  ngOnInit() {
    this.newGameForm = this.fb.group({
      bank: [12000],
      ipoDividendToTreasury: [false],
      poolDividendToTreasury: [false],
      treasuryShareDividendToTreasury: [true],
      startingCash: [0],
      currencySymbol: ['$'],
      defaultShareSource: [SHARE_SOURCE.IPO],
      disableIPO: [false],
      companiesCanOwnOtherCompanies: [false],
      shortingAllowed: [false],
      canSplitRevenue: [true],
      players: this.fb.array([
          this.fb.control(''),
          this.fb.control(''),
          this.fb.control('')
        ],
        this.playerNumberValidator)
    }, {
      validator: ipoDisabledValidator
    });

    this.newGameForm.controls['disableIPO'].valueChanges
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(value => {
        if (value && this.newGameForm.get('defaultShareSource').value === SHARE_SOURCE.IPO) {
          this.newGameForm.patchValue({
            defaultShareSource: SHARE_SOURCE.TREASURY
          });
        }
      });
  }

  createNewGame(e) {
    e.preventDefault();
    const gameId = this.es.generateId();
    console.log('stream id', gameId);
    const startGameEvent = new SettingsChangedEvent(
      this.newGameForm.get('bank').value,
      this.newGameForm.get('ipoDividendToTreasury').value,
      this.newGameForm.get('poolDividendToTreasury').value,
      this.newGameForm.get('treasuryShareDividendToTreasury').value,
      this.newGameForm.get('currencySymbol').value,
      this.newGameForm.get('defaultShareSource').value,
      this.newGameForm.get('disableIPO').value,
      this.newGameForm.get('companiesCanOwnOtherCompanies').value,
      this.newGameForm.get('shortingAllowed').value,
      this.newGameForm.get('canSplitRevenue').value,
    );
    const playerEvents: GameEvent[] = this.players.controls
      .filter(player => !!player.value.trim())
      .map(player =>
        new PlayerAddedEvent(
          player.value.trim(),
          this.newGameForm.get('startingCash').value
        )
      );
    let events: GameEvent[] = (<GameEvent[]>[startGameEvent]).concat(playerEvents);
    // TODO: add privates when they are reimplemented
    events = events.concat(this.companies.map(
      c => new CompanyAddedEvent(
        c.name, c.startingCash, c.shares, c.textColor, c.backgroundColor,
        c.shareSource ? c.shareSource : this.newGameForm.get('defaultShareSource').value,
      )
    ));
    this.es.postEvents(gameId, events)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(() => this.router.navigate(['overview', gameId]));
    this.es.registerGame(gameId)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(() => {});
  }

  removePlayer(i: number) {
    this.newGameForm.get('players').removeAt(i);
  }

  addPlayer() {
    this.players.push(this.fb.control(''));
  }

  removePrivate(i: number) {
    this.privates.splice(i, 1);
  }

  removeCompany(i: number) {
    this.companies.splice(i, 1);
  }

  selectPreset(name: string) {
    this.selectedPreset = name;
    const preset = this.PRESETS[name];
    this.newGameForm.patchValue(this.PRESETS[name]);
    this.newGameForm.patchValue({
      startingCash: preset.calculateStartingCash(this.players.controls.filter(p => !!p.value.trim()).length)
    });
    this.companies = preset.companies;
    this.privates = preset.privates;
    this.newGameForm.controls['players'].updateValueAndValidity();
  }

  playerNumberValidator: ValidatorFn = (players: FormArray): ValidationErrors | null => {
    const playerCount = players.controls.filter(player => !!player.value.trim()).length ;
    if (this.selectedPreset && playerCount < this.PRESETS[this.selectedPreset].minimumPlayerCount) {
      return { tooFewPlayers: true };
    } else if (this.selectedPreset === undefined && playerCount < MIN_PLAYERS) {
      return { tooFewPlayers: true };
    }
    return null;
  }
}
