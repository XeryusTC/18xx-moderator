import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';

import { CompanyColorsDirective } from '../game/common-elements/company-colors/company-colors.directive';
import { MenuComponent } from '../menu/menu.component';
import { NewGameComponent } from './new-game.component';
import { EventStoreService } from '../services/event-store.service';

describe('NewGameComponent', () => {
  let component: NewGameComponent;
  let fixture: ComponentFixture<NewGameComponent>;
  const routerSpy = jasmine.createSpyObj('Router', ['navigate']);
  const eventStoreServiceStub: Partial<EventStoreService> = {};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule
      ],
      declarations: [
        CompanyColorsDirective,
        MenuComponent,
        NewGameComponent
      ],
      providers: [
        { provide: Router, useValue: routerSpy },
        { provide: EventStoreService, useValue: eventStoreServiceStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
