import { GameState } from './state';
import {
  CompanyAddedEvent,
  CompanyEditedEvent,
  CompanyRemovedEvent,
  DIVIDEND_METHOD,
  MoneyTransferredEvent,
  OperatedEvent,
  PlayerAddedEvent,
  PrivateAcquiredEvent,
  PrivateAddedEvent,
  PrivateRemovedEvent,
  PrivateRevenueEvent,
  SettingsChangedEvent,
  SHARE_SOURCE,
  ShareTransferredEvent
} from './events';

describe('SettingsChangedEvent', () => {
  let state;

  it('should set the settings', () => {
    state = new GameState();
    expect(state.settings).toBeFalsy();
    state = new SettingsChangedEvent(12000, false, true, false, '$', SHARE_SOURCE.IPO, false, true, false, true)
      .apply(state);
    expect(state.settings.canSplitRevenue).toBe(true);
    expect(state.settings.companiesCanOwnOtherCompanies).toBe(true);
    expect(state.settings.currencySymbol).toBe('$');
    expect(state.settings.defaultShareSource).toBe(SHARE_SOURCE.IPO);
    expect(state.settings.disableIPO).toBe(false);
    expect(state.settings.ipoPaysDividend).toBe(false);
    expect(state.settings.poolPaysDividend).toBe(true);
    expect(state.settings.shortingAllowed).toBe(false);
    expect(state.settings.treasuryPaysDividend).toBe(false);
  });

  it('overwrites old settings', () => {
    state = [
      new SettingsChangedEvent(1, false, false, false, '$', SHARE_SOURCE.IPO, false, false, false, false),
      new SettingsChangedEvent(2, true, true, true, '#', SHARE_SOURCE.POOL, true, true, true, true)
    ].reduce((s, event) => event.apply(s), new GameState());
    expect(state.settings.canSplitRevenue).toBe(true);
    expect(state.settings.companiesCanOwnOtherCompanies).toBe(true);
    expect(state.settings.currencySymbol).toBe('#');
    expect(state.settings.defaultShareSource).toBe(SHARE_SOURCE.POOL);
    expect(state.settings.disableIPO).toBe(true);
    expect(state.settings.ipoPaysDividend).toBe(true);
    expect(state.settings.poolPaysDividend).toBe(true);
    expect(state.settings.shortingAllowed).toBe(true);
    expect(state.settings.treasuryPaysDividend).toBe(true);
  });
});

describe('PlayerAddedEvent', () => {
  let state: GameState;

  beforeEach(() => {
    state = [
      new SettingsChangedEvent(100, true, true, true, '$', SHARE_SOURCE.POOL, true, true, true, true),
    ].reduce((s, event) => event.apply(s), new GameState());
  });

  it('should add a player', () => {
    state = [ new PlayerAddedEvent('Alice', 100) ].reduce((s, event) => event.apply(s), state);
    expect(state.players.hasOwnProperty('Alice')).toBe(true);
    expect(state.players['Alice'].cash).toBe(100);
  });

  it('should be idempotent', () => {
    state = [
      new PlayerAddedEvent('Bob', 100),
      new PlayerAddedEvent('Bob', 10)
    ].reduce((s, event) => event.apply(s), state);
    expect(state.players.hasOwnProperty('Bob')).toBe(true);
    expect(state.players['Bob'].cash).toBe(100);
  });

  it('does add multiple players', () => {
    state = [
      new PlayerAddedEvent('Charlie', 10),
      new PlayerAddedEvent('Dave', 10)
    ].reduce((s, event) => event.apply(s), state);
    expect(state.players.hasOwnProperty('Charlie')).toBe(true);
    expect(state.players.hasOwnProperty('Dave')).toBe(true);
  });
});

describe('CompanyAddedEvent', () => {
  let state;

  it('should add a company', () => {
    state = [
      new SettingsChangedEvent(100, false, false, false, '$', SHARE_SOURCE.IPO, false, false, false, false)
    ].reduce((s, event) => event.apply(s), new GameState());
    state = new CompanyAddedEvent('B&O', 50, 10, '#fff', '#000', SHARE_SOURCE.IPO).apply(state);
    expect(state.companies.hasOwnProperty('B&O'));
    expect(state.companies['B&O'].cash).toBe(50, 'cash is incorrect');
    expect(state.companies['B&O'].ipoShares).toBe(10, 'IPO shares are incorrect');
    expect(state.companies['B&O'].poolShares).toBe(0, 'Pool shares are incorrect');
    expect(state.companies['B&O'].textColor).toBe('#fff', 'Text color is incorrect');
    expect(state.companies['B&O'].backgroundColor).toBe('#000', 'Background color is incorrect');
  });

  it('should be idempotent', () => {
    state = [
      new SettingsChangedEvent(100, false, false, false, '$', SHARE_SOURCE.IPO, false, false, false, true),
      new CompanyAddedEvent('LNWR', 0, 10, '#fff', '#000', SHARE_SOURCE.IPO)
    ].reduce((s, event) => event.apply(s), new GameState());
    state = new CompanyAddedEvent('LNWR', 100, 5, '#00f', '#f00', SHARE_SOURCE.POOL).apply(state);
    expect(state.companies['LNWR'].cash).toBe(0, 'cash is incorrect');
    expect(state.companies['LNWR'].ipoShares).toBe(10, 'IPO shares is incorrect');
    expect(state.companies['LNWR'].poolShares).toBe(0, 'Pool shares is incorrect');
    expect(state.companies['LNWR'].textColor).toBe('#fff', 'Incorrect text color');
    expect(state.companies['LNWR'].backgroundColor).toBe('#000', 'Incorrect background color');
  });

  it('should re-enable removed companies', () => {
    state = [
      new SettingsChangedEvent(100, false, false, false, '$', SHARE_SOURCE.IPO, false, false, false, false),
      new CompanyAddedEvent('CPR', 50, 10, '#000', '#f00', SHARE_SOURCE.IPO),
      new CompanyRemovedEvent('CPR')
    ].reduce((s, event) => event.apply(s), new GameState());
    expect(state.companies['CPR'].active).toBe(false);
    state = new CompanyAddedEvent('CPR', 20, 2, '#fff', '#123', SHARE_SOURCE.POOL).apply(state);
    expect(state.companies['CPR'].cash).toBe(20, 'cash is incorrect');
    expect(state.companies['CPR'].ipoShares).toBe(0, 'IPO shares are incorrect');
    expect(state.companies['CPR'].poolShares).toBe(2, 'Pool shares are incorrect');
    expect(state.companies['CPR'].textColor).toBe('#fff', 'Text color is incorrect');
    expect(state.companies['CPR'].backgroundColor).toBe('#123', 'Background color is incorrect');
    expect(state.companies['CPR'].active).toBe(true);
  });

  it('should put shares in the company when when share source is treasury', () => {
    state = [
      new SettingsChangedEvent(100, false, false, false, '$', SHARE_SOURCE.IPO, false, false, false, false),
      new CompanyAddedEvent('CPR', 50, 20, '#000', '#f00', SHARE_SOURCE.TREASURY)
    ].reduce((s, event) => event.apply(s), new GameState());
    expect(state.allCompanies['CPR'].shares.hasOwnProperty('CPR')).toBe(true);
    expect(state.allCompanies['CPR'].shares['CPR']).toBe(20);
  });
});

describe('CompanyRemovedEvent', () => {
  let state: GameState;

  beforeEach(() => {
    state = [
      new SettingsChangedEvent(100, true, true, true, '$', SHARE_SOURCE.IPO, true, true, true, true),
      new CompanyAddedEvent('CR', 100, 10, '#000', '#00f', SHARE_SOURCE.IPO)
    ].reduce((s, event) => event.apply(s), new GameState());
  });

  it('should set company state to inactive', () => {
    expect(Object.keys(state.activeCompanies)).toContain('CR', 'Company is not in active object');
    expect(state.allCompanies['CR'].active).toBe(true);
    state = new CompanyRemovedEvent('CR').apply(state);
    expect(Object.keys(state.disabledCompanies)).toContain('CR', 'Company is not in disabled object');
    expect(state.allCompanies['CR'].active).toBe(false);
  });

  it('should remove shares from players', () => {
    state = [
      new PlayerAddedEvent('Alice', 0),
      new ShareTransferredEvent('Alice', SHARE_SOURCE.IPO, 'CR', 2, 0)
    ].reduce((s, event) => event.apply(s), state);
    expect(state.players['Alice'].shares.hasOwnProperty('CR')).toBe(true);
    expect(state.players['Alice'].shares['CR']).toBe(2);
    state = new CompanyRemovedEvent('CR').apply(state);
    expect(state.players['Alice'].shares.hasOwnProperty('CR')).toBe(false);
  });

  it('should remove shares from companies', () => {
    state = [
      new CompanyAddedEvent('LYR', 0, 10, '#fff', '#909', SHARE_SOURCE.IPO),
      new ShareTransferredEvent('LYR', SHARE_SOURCE.IPO, 'CR', 3, 0)
    ].reduce((s, event) => event.apply(s), state);
    expect(state.allCompanies['LYR'].shares.hasOwnProperty('CR')).toBe(true);
    expect(state.allCompanies['LYR'].shares['CR']).toBe(3);
    state = new CompanyRemovedEvent('CR').apply(state);
    expect(state.allCompanies['LYR'].shares.hasOwnProperty('CR')).toBe(false);
  });

  it('should put shares owned by the company into the pool', () => {
    state = [
      new CompanyAddedEvent('NBR', 0, 10, '#000', '#6f6', SHARE_SOURCE.IPO),
      new ShareTransferredEvent('CR', SHARE_SOURCE.IPO, 'NBR', 4, 0)
    ].reduce((s, event) => event.apply(s), state);
    expect(state.allCompanies['CR'].shares.hasOwnProperty('NBR')).toBe(true);
    expect(state.allCompanies['CR'].shares['NBR']).toBe(4);
    expect(state.allCompanies['NBR'].poolShares).toBe(0);
    state = new CompanyRemovedEvent('CR').apply(state);
    expect(state.allCompanies['NBR'].poolShares).toBe(4);
  });
});

describe('CompanyEditedEvent', () => {
  let state: GameState;

  beforeEach(() => {
    state = [
      new SettingsChangedEvent(100, true, true, true, '$', SHARE_SOURCE.IPO, true, true, true, true),
      new CompanyAddedEvent('LNWR', 0, 10, '#fff', '#000', SHARE_SOURCE.IPO),
    ].reduce((s, event) => event.apply(s), new GameState());
  });

  it('should change company properties', () => {
    state = new CompanyEditedEvent('LNWR', 10, SHARE_SOURCE.IPO, '#666', '#999').apply(state);
    expect(state.allCompanies['LNWR'].textColor).toBe('#666');
    expect(state.allCompanies['LNWR'].backgroundColor).toBe('#999');
  });

  it('should put extra shares in the IPO when changing share size', () => {
    state = new CompanyEditedEvent('LNWR', 15, SHARE_SOURCE.IPO, '#000', '#fff').apply(state);
    expect(state.allCompanies['LNWR'].ipoShares).toBe(15);
    expect(state.allCompanies['LNWR'].poolShares).toBe(0);
    expect(state.allCompanies['LNWR'].shares['LNWR']).toBeFalsy();
  });

  it('should put extra shares in the pool when changing share size', () => {
    state = new CompanyEditedEvent('LNWR', 15, SHARE_SOURCE.POOL, '#000', '#fff').apply(state);
    expect(state.allCompanies['LNWR'].ipoShares).toBe(10);
    expect(state.allCompanies['LNWR'].poolShares).toBe(5);
    expect(state.allCompanies['LNWR'].shares['LNWR']).toBeFalsy();
  });

  it('should put extra shares in the treasury when changing share size', () => {
    state = new CompanyEditedEvent('LNWR', 13, SHARE_SOURCE.TREASURY, '#000', '#fff').apply(state);
    expect(state.allCompanies['LNWR'].ipoShares).toBe(10);
    expect(state.allCompanies['LNWR'].poolShares).toBe(0);
    expect(state.allCompanies['LNWR'].shares['LNWR']).toBe(3);
  });

  it('should remove extra shares from the IPO when changing share size', () => {
    state = new CompanyEditedEvent('LNWR', 9, SHARE_SOURCE.IPO, '#000', '#fff').apply(state);
    expect(state.allCompanies['LNWR'].ipoShares).toBe(9);
    expect(state.allCompanies['LNWR'].poolShares).toBe(0);
    expect(state.allCompanies['LNWR'].shares['LNWR']).toBeFalsy();
  });

  it('should remove extra shares from the pool when changing share size', () => {
    state = new CompanyEditedEvent('LNWR', 8, SHARE_SOURCE.POOL, '#000', '#fff').apply(state);
    expect(state.allCompanies['LNWR'].ipoShares).toBe(10);
    expect(state.allCompanies['LNWR'].poolShares).toBe(-2);
    expect(state.allCompanies['LNWR'].shares['LNWR']).toBeFalsy();
  });

  it('should remove extra shares from the treasury when changing share size', () => {
    state = new CompanyEditedEvent('LNWR', 7, SHARE_SOURCE.TREASURY, '#000', '#fff').apply(state);
    expect(state.allCompanies['LNWR'].ipoShares).toBe(10);
    expect(state.allCompanies['LNWR'].poolShares).toBe(0);
    expect(state.allCompanies['LNWR'].shares['LNWR']).toBe(-3);
  });
});

describe('PrivateAddedEvent', () => {
  let state: GameState;

  it('should add a private', () => {
    state = new PrivateAddedEvent('SV', 5).apply(new GameState());
    expect(state.privates.hasOwnProperty('SV'));
    expect(state.privates['SV'].revenue).toBe(5);
  });

  it('should be idempotent', () => {
    state = [
      new PrivateAddedEvent('C&StL', 10)
    ].reduce((s, event) => event.apply(s), new GameState());
    state = new PrivateAddedEvent('C&StL', 30).apply(state);
    expect(state.privates['C&StL'].revenue).toBe(10);
  });
});

describe('PrivateRemovedEvent', () => {
  let state: GameState;

  beforeEach(() => {
    state = [
      new PrivateAddedEvent('P', 10)
    ].reduce((s, event) => event.apply(s), new GameState());
  });

  it('should set private state to inactive', () => {
    state = new PrivateRemovedEvent('P').apply(state);
    expect(state.privates['P'].active).toBe(false);
  });

  it('should not fail when removing non-existent private', () => {
    state = new PrivateRemovedEvent('DoesNotExist').apply(state);
  });
});

describe('PrivateAcquiredEvent', () => {
  let state: GameState;

  beforeEach(() => {
    state = [
      new SettingsChangedEvent(1000, true, true, true, '', SHARE_SOURCE.IPO, true, true, true, true),
      new PlayerAddedEvent('Player1', 100),
      new PlayerAddedEvent('Player2', 100),
      new CompanyAddedEvent('Cie1', 100, 10, '', '', SHARE_SOURCE.IPO),
      new CompanyAddedEvent('Cie2', 100, 10, '', '', SHARE_SOURCE.IPO),
      new PrivateAddedEvent('P', 10)
    ].reduce((s, event) => event.apply(s), new GameState());
  });

  it('player acquires private from pool', () => {
    state = new PrivateAcquiredEvent('P', 'Player1', 10).apply(state);
    expect(state.bank).toBe(610);
    expect(state.players['Player1'].cash).toBe(90);
    expect(state.privates['P'].owner).toBe('Player1');
  });

  it('player acquires private from other player', () => {
    state = [
      new PrivateAcquiredEvent('P', 'Player2', 0),
      new PrivateAcquiredEvent('P', 'Player1', 20)
    ].reduce((s, event) => event.apply(s), state);
    expect(state.bank).toBe(600);
    expect(state.players['Player1'].cash).toBe(80);
    expect(state.players['Player2'].cash).toBe(120);
    expect(state.privates['P'].owner).toBe('Player1');
  });

  it('player acquires private from company', () => {
    state = [
      new PrivateAcquiredEvent('P', 'Cie1', 0),
      new PrivateAcquiredEvent('P', 'Player1', 30)
    ].reduce((s, event) => event.apply(s), state);
    expect(state.bank).toBe(600);
    expect(state.players['Player1'].cash).toBe(70);
    expect(state.allCompanies['Cie1'].cash).toBe(130);
    expect(state.privates['P'].owner).toBe('Player1');
  });

  it('company acquires private from pool', () => {
    state = new PrivateAcquiredEvent('P', 'Cie1', 40).apply(state);
    expect(state.bank).toBe(640);
    expect(state.allCompanies['Cie1'].cash).toBe(60);
    expect(state.privates['P'].owner).toBe('Cie1');
  });

  it('company acquires private from player', () => {
    state = [
      new PrivateAcquiredEvent('P', 'Player1', 0),
      new PrivateAcquiredEvent('P', 'Cie1', 50)
    ].reduce((s, event) => event.apply(s), state);
    expect(state.bank).toBe(600);
    expect(state.players['Player1'].cash).toBe(150);
    expect(state.allCompanies['Cie1'].cash).toBe(50);
    expect(state.privates['P'].owner).toBe('Cie1');
  });

  it('company acquires private from company', () => {
    state = [
      new PrivateAcquiredEvent('P', 'Cie1', 0),
      new PrivateAcquiredEvent('P', 'Cie2', 60)
    ].reduce((s, event) => event.apply(s), state);
    expect(state.bank).toBe(600);
    expect(state.allCompanies['Cie1'].cash).toBe(160);
    expect(state.allCompanies['Cie2'].cash).toBe(40);
    expect(state.privates['P'].owner).toBe('Cie2');
  });
});

describe('PrivateRevenueEvent', () => {
  let state: GameState;

  beforeEach(() => {
    state = [
      new SettingsChangedEvent(1000, false, false, false, '', SHARE_SOURCE.IPO, false, false, false, false),
      new PrivateAddedEvent('P', 15),
      new PlayerAddedEvent('Player', 30),
      new CompanyAddedEvent('Cie', 45, 10, '', '', SHARE_SOURCE.IPO)
    ].reduce((s, event) => event.apply(s), new GameState());
  });

  it('private with no owner pays no revenue', () => {
    state = new PrivateRevenueEvent('P').apply(state);
    expect(state.bank).toBe(925);
  });

  it('private pays revenue to player', () => {
    state = [
      new PrivateAcquiredEvent('P', 'Player', 0),
      new PrivateRevenueEvent('P')
    ].reduce((s, event) => event.apply(s), state);
    expect(state.bank).toBe(910);
    expect(state.players['Player'].cash).toBe(45);
  });

  it('private pays revenue to company', () => {
    state = [
      new PrivateAcquiredEvent('P', 'Cie', 0),
      new PrivateRevenueEvent('P')
    ].reduce((s, event) => event.apply(s), state);
    expect(state.bank).toBe(910);
    expect(state.allCompanies['Cie'].cash).toBe(60);
  });
});

describe('MoneyTransferredEvent', () => {
  let state: GameState;

  beforeEach(() => {
    state = [
      new SettingsChangedEvent(1000, true, true, true, '$', SHARE_SOURCE.IPO, false, true, true, true),
      new CompanyAddedEvent('Cie1', 100, 10, '', '', SHARE_SOURCE.IPO),
      new CompanyAddedEvent('Cie2', 100, 10, '', '', SHARE_SOURCE.IPO),
      new PlayerAddedEvent('Player1', 100),
      new PlayerAddedEvent('Player2', 100)
    ].reduce((s, event) => event.apply(s), new GameState());
    expect(state.bank).toBe(600);
  });

  it('should transfer money from bank to player', () => {
    state = new MoneyTransferredEvent('bank', 'Player1', 1).apply(state);
    expect(state.bank).toBe(599);
    expect(state.players['Player1'].cash).toBe(101);
  });

  it('should transfer money from bank to company', () => {
    state = new MoneyTransferredEvent('bank', 'Cie1', 2).apply(state);
    expect(state.bank).toBe(598);
    expect(state.allCompanies['Cie1'].cash).toBe(102);
  });

  it('should transfer money from player to bank', () => {
    state = new MoneyTransferredEvent('Player1', 'bank', 3).apply(state);
    expect(state.bank).toBe(603);
    expect(state.players['Player1'].cash).toBe(97);
  });

  it('should transfer money from player to player', () => {
    state = new MoneyTransferredEvent('Player1', 'Player2', 4).apply(state);
    expect(state.players['Player1'].cash).toBe(96);
    expect(state.players['Player2'].cash).toBe(104);
  });

  it('should transfer money from player to company', () => {
    state = new MoneyTransferredEvent('Player1', 'Cie1', 5).apply(state);
    expect(state.players['Player1'].cash).toBe(95);
    expect(state.allCompanies['Cie1'].cash).toBe(105);
  });

  it('should transfer money from company to bank', () => {
    state = new MoneyTransferredEvent('Cie1', 'bank', 6).apply(state);
    expect(state.allCompanies['Cie1'].cash).toBe(94);
    expect(state.bank).toBe(606);
  });

  it('should transfer money from company to player', () => {
    state = new MoneyTransferredEvent('Cie1', 'Player1', 7).apply(state);
    expect(state.allCompanies['Cie1'].cash).toBe(93);
    expect(state.players['Player1'].cash).toBe(107);
  });

  it('should transfer money from company to company', () => {
    state = new MoneyTransferredEvent('Cie1', 'Cie2', 8).apply(state);
    expect(state.allCompanies['Cie1'].cash).toBe(92);
    expect(state.allCompanies['Cie2'].cash).toBe(108);
  });
});

describe('OperatedEvent', () => {
  let state;

  describe('2-share company', () => {
    beforeEach(() => {
      state = [
        new SettingsChangedEvent(1000, false, false, false, '$', SHARE_SOURCE.IPO, false, true, true, true),
        new CompanyAddedEvent('B&O', 0, 2, '', '', SHARE_SOURCE.IPO),
        new PlayerAddedEvent('Alice', 0),
        new ShareTransferredEvent('Alice', SHARE_SOURCE.IPO, 'B&O', 2, 0)
      ].reduce((s, event) => event.apply(s), new GameState());
    });

    it('full dividend', () => {
      state = new OperatedEvent('B&O', [100], DIVIDEND_METHOD.FULL).apply(state);
      expect(state.players['Alice'].cash).toBe(100);
      expect(state.companies['B&O'].cash).toBe(0);
      expect(state.bank).toBe(900);
    });

    it('split 1', () => {
      state = new OperatedEvent('B&O', [70], DIVIDEND_METHOD.SPLIT).apply(state);
      expect(state.players['Alice'].cash).toBe(35);
      expect(state.companies['B&O'].cash).toBe(35);
      expect(state.bank).toBe(930);
    });

    it('split 2', () => {
      state = new OperatedEvent('B&O', [120], DIVIDEND_METHOD.SPLIT).apply(state);
      expect(state.players['Alice'].cash).toBe(60);
      expect(state.companies['B&O'].cash).toBe(60);
      expect(state.bank).toBe(880);
    });

    it(DIVIDEND_METHOD.WITHHOLD, () => {
      state = new OperatedEvent('B&O', [200], DIVIDEND_METHOD.WITHHOLD).apply(state);
      expect(state.players['Alice'].cash).toBe(0);
      expect(state.companies['B&O'].cash).toBe(200);
      expect(state.bank).toBe(800);
    });
  });

  describe('5-share company', () => {
    beforeEach(() => {
      state = [
        new SettingsChangedEvent(1000, false, false, true, '$', SHARE_SOURCE.IPO, false, true, true, true),
        new CompanyAddedEvent('PRR', 0, 5, '', '', SHARE_SOURCE.IPO),
        new PlayerAddedEvent('Alice', 0),
        new PlayerAddedEvent('Bob', 0),
        new ShareTransferredEvent('Alice', SHARE_SOURCE.IPO, 'PRR', 3, 0),
        new ShareTransferredEvent('Bob', SHARE_SOURCE.IPO, 'PRR', 1, 0)
      ].reduce((s, event) => event.apply(s), new GameState());
    });

    it('full dividend', () => {
      state = new OperatedEvent('PRR', [40], DIVIDEND_METHOD.FULL).apply(state);
      expect(state.players['Alice'].cash).toBe(24);
      expect(state.players['Bob'].cash).toBe(8);
      expect(state.companies['PRR'].cash).toBe(0);
      expect(state.bank).toBe(968);
    });

    it('split 1', () => {
      state = new OperatedEvent('PRR', [90], DIVIDEND_METHOD.SPLIT).apply(state);
      expect(state.players['Alice'].cash).toBe(27);
      expect(state.players['Bob'].cash).toBe(9);
      expect(state.companies['PRR'].cash).toBe(45);
      expect(state.bank).toBe(919);
    });

    it('split 2', () => {
      state = new OperatedEvent('PRR', [100], DIVIDEND_METHOD.SPLIT).apply(state);
      expect(state.players['Alice'].cash).toBe(30);
      expect(state.players['Bob'].cash).toBe(10);
      expect(state.companies['PRR'].cash).toBe(50);
      expect(state.bank).toBe(910);
    });

    it(DIVIDEND_METHOD.WITHHOLD, () => {
      state = new OperatedEvent('PRR', [80], DIVIDEND_METHOD.WITHHOLD).apply(state);
      expect(state.players['Alice'].cash).toBe(0);
      expect(state.players['Bob'].cash).toBe(0);
      expect(state.companies['PRR'].cash).toBe(80);
      expect(state.bank).toBe(920);
    });
  });

  describe('10-share company with Pool share', () => {
    beforeEach(() => {
      state = [
        new SettingsChangedEvent(1000, false, false, false, '$', SHARE_SOURCE.IPO, false, true, true, true),
        new CompanyAddedEvent('C&O', 0, 10, '', '', SHARE_SOURCE.IPO),
        new PlayerAddedEvent('Alice', 0),
        new PlayerAddedEvent('Bob', 0),
        new PlayerAddedEvent('Charlie', 0),
        new ShareTransferredEvent('Alice', SHARE_SOURCE.IPO, 'C&O', 5, 0),
        new ShareTransferredEvent('Bob', SHARE_SOURCE.IPO, 'C&O', 2, 0),
        new ShareTransferredEvent('Charlie', SHARE_SOURCE.IPO, 'C&O', 1, 0)
      ].reduce((s, event) => event.apply(s), new GameState());
    });

    it('full dividend', () => {
      state = new OperatedEvent('C&O', [230], DIVIDEND_METHOD.FULL).apply(state);
      expect(state.players['Alice'].cash).toBe(115);
      expect(state.players['Bob'].cash).toBe(46);
      expect(state.players['Charlie'].cash).toBe(23);
      expect(state.companies['C&O'].cash).toBe(0);
      expect(state.bank).toBe(816);
    });

    it('split 1', () => {
      state = new OperatedEvent('C&O', [170], DIVIDEND_METHOD.SPLIT).apply(state);
      expect(state.players['Alice'].cash).toBe(45);
      expect(state.players['Bob'].cash).toBe(18);
      expect(state.players['Charlie'].cash).toBe(9);
      expect(state.companies['C&O'].cash).toBe(80);
      expect(state.bank).toBe(848);
    });

    it('split 2', () => {
      state = new OperatedEvent('C&O', [200], DIVIDEND_METHOD.SPLIT).apply(state);
      expect(state.players['Alice'].cash).toBe(50);
      expect(state.players['Bob'].cash).toBe(20);
      expect(state.players['Charlie'].cash).toBe(10);
      expect(state.companies['C&O'].cash).toBe(100);
      expect(state.bank).toBe(820);
    });

    it(DIVIDEND_METHOD.WITHHOLD, () => {
      state = new OperatedEvent('C&O', [30], DIVIDEND_METHOD.WITHHOLD).apply(state);
      expect(state.players['Alice'].cash).toBe(0);
      expect(state.players['Bob'].cash).toBe(0);
      expect(state.players['Charlie'].cash).toBe(0);
      expect(state.companies['C&O'].cash).toBe(30);
      expect(state.bank).toBe(970);
    });
  });

  describe('IPO shares pay', () => {
    beforeEach(() => {
      state = [
        new SettingsChangedEvent(1000, true, false, false, '$', SHARE_SOURCE.IPO, false, true, true, true),
        new CompanyAddedEvent('Erie', 0, 10, '', '', SHARE_SOURCE.IPO),
        new PlayerAddedEvent('Alice', 0),
        new ShareTransferredEvent('Alice', SHARE_SOURCE.IPO, 'Erie', 5, 0),
        new ShareTransferredEvent('pool', SHARE_SOURCE.IPO, 'Erie', 2, 0)
      ].reduce((s, event) => event.apply(s), new GameState());
    });

    it('full dividend', () => {
      state = new OperatedEvent('Erie', [130], DIVIDEND_METHOD.FULL).apply(state);
      expect(state.players['Alice'].cash).toBe(65);
      expect(state.companies['Erie'].cash).toBe(39);
      expect(state.bank).toBe(896);
    });

    it('split 1', () => {
      state = new OperatedEvent('Erie', [150], DIVIDEND_METHOD.SPLIT).apply(state);
      expect(state.players['Alice'].cash).toBe(40);
      expect(state.companies['Erie'].cash).toBe(94);
      expect(state.bank).toBe(866);
    });

    it('split 2', () => {
      state = new OperatedEvent('Erie', [100], DIVIDEND_METHOD.SPLIT).apply(state);
      expect(state.players['Alice'].cash).toBe(25);
      expect(state.companies['Erie'].cash).toBe(65);
      expect(state.bank).toBe(910);
    });
  });

  describe('Pool shares pay', () => {
    beforeEach(() => {
      state = [
        new SettingsChangedEvent(1000, false, true, false, '$', SHARE_SOURCE.IPO, false, true, true, true),
        new CompanyAddedEvent('B&M', 0, 10, '', '', SHARE_SOURCE.IPO),
        new PlayerAddedEvent('Alice', 0),
        new ShareTransferredEvent('Alice', SHARE_SOURCE.IPO, 'B&M', 5, 0),
        new ShareTransferredEvent('pool', SHARE_SOURCE.IPO, 'B&M', 2, 0)
      ].reduce((s, event) => event.apply(s), new GameState());
    });

    it('full dividend', () => {
      state = new OperatedEvent('B&M', [110], DIVIDEND_METHOD.FULL).apply(state);
      expect(state.players['Alice'].cash).toBe(55);
      expect(state.companies['B&M'].cash).toBe(22);
      expect(state.bank).toBe(923);
    });

    it('split 1', () => {
      state = new OperatedEvent('B&M', [210], DIVIDEND_METHOD.SPLIT).apply(state);
      expect(state.players['Alice'].cash).toBe(55);
      expect(state.companies['B&M'].cash).toBe(122);
      expect(state.bank).toBe(823);
    });

    it('split 2', () => {
      state = new OperatedEvent('B&M', [300], DIVIDEND_METHOD.SPLIT).apply(state);
      expect(state.players['Alice'].cash).toBe(75);
      expect(state.companies['B&M'].cash).toBe(180);
      expect(state.bank).toBe(745);
    });
  });

  describe('treasury pays:', () => {
    beforeEach(() => {
      state = [
        new SettingsChangedEvent(1000, false, false, true, '$', SHARE_SOURCE.IPO, false, true, true, true),
        new CompanyAddedEvent('CPR', 0, 10, '', '', SHARE_SOURCE.TREASURY),
        new CompanyAddedEvent('NYC', 0, 10, '', '', SHARE_SOURCE.IPO),
        new PlayerAddedEvent('Alice', 0),
        new ShareTransferredEvent('Alice', 'CPR', 'CPR', 3, 0),
        new ShareTransferredEvent('NYC', 'CPR', 'CPR', 2, 0)
      ].reduce((s, event) => event.apply(s), new GameState());
    });

    it('full dividend', () => {
      state = new OperatedEvent('CPR', [550], DIVIDEND_METHOD.FULL).apply(state);
      expect(state.players['Alice'].cash).toBe(165);
      expect(state.companies['CPR'].cash).toBe(275);
      expect(state.companies['NYC'].cash).toBe(110);
      expect(state.bank).toBe(450);
    });

    it('split 1', () => {
      state = new OperatedEvent('CPR', [410], DIVIDEND_METHOD.SPLIT).apply(state);
      expect(state.players['Alice'].cash).toBe(63);
      expect(state.companies['CPR'].cash).toBe(305);
      expect(state.companies['NYC'].cash).toBe(42);
      expect(state.bank).toBe(590);
    });

    it('split 2', () => {
      state = new OperatedEvent('CPR', [360], DIVIDEND_METHOD.SPLIT).apply(state);
      expect(state.players['Alice'].cash).toBe(54);
      expect(state.companies['CPR'].cash).toBe(270);
      expect(state.companies['NYC'].cash).toBe(36);
      expect(state.bank).toBe(640);
    });
  });

  it('treasury doesn\'t pay', () => {
    state = [
      new SettingsChangedEvent(1000, false, false, false, '$', SHARE_SOURCE.IPO, false, true, true, true),
      new CompanyAddedEvent('PMQ', 0, 10, '', '', SHARE_SOURCE.IPO),
      new CompanyAddedEvent('NYNH', 0, 10, '', '', SHARE_SOURCE.IPO),
      new PlayerAddedEvent('Alice', 0),
      new ShareTransferredEvent('Alice', SHARE_SOURCE.IPO, 'PMQ', 2, 0),
      new ShareTransferredEvent('PMQ', SHARE_SOURCE.IPO, 'PMQ', 4, 0),
      new ShareTransferredEvent('NYNH', SHARE_SOURCE.IPO, 'PMQ', 3, 0),
      new OperatedEvent('PMQ', [10], DIVIDEND_METHOD.FULL)
    ].reduce((s, event) => event.apply(s), new GameState());
    expect(state.players['Alice'].cash).toBe(2);
    expect(state.companies['PMQ'].cash).toBe(0);
    expect(state.companies['NYNH'].cash).toBe(0);
    expect(state.bank).toBe(998);
  });
});

describe('ShareTransferredEvent', () => {
  let state: GameState;

  describe('From and to IPO', () => {
    beforeEach(() => {
      state = [
        new SettingsChangedEvent(1000, true, true, true, '$', SHARE_SOURCE.IPO, false, true, true, true),
        new PlayerAddedEvent('Alice', 100),
        new CompanyAddedEvent('SWR', 100, 10, '', '', SHARE_SOURCE.IPO),
        new CompanyAddedEvent('MR', 100, 10, '', '', SHARE_SOURCE.IPO)
      ].reduce((s, event) => event.apply(s), new GameState());
    });

    it('Player buy share from IPO', () => {
      state = new ShareTransferredEvent('Alice', SHARE_SOURCE.IPO, 'SWR', 2, 10).apply(state);
      expect(state.players['Alice'].shares.hasOwnProperty('SWR')).toBe(true);
      expect(state.players['Alice'].shares['SWR']).toBe(2);
      expect(state.players['Alice'].cash).toBe(80);
      expect(state.allCompanies['SWR'].ipoShares).toBe(8);
      expect(state.allCompanies['SWR'].cash).toBe(100);
      expect(state.bank).toBe(720);
    });

    it('Player sells share to IPO', () => {
      state = new ShareTransferredEvent('Alice', SHARE_SOURCE.IPO, 'SWR', -4, 10).apply(state);
      expect(state.players['Alice'].shares.hasOwnProperty('SWR')).toBe(true);
      expect(state.players['Alice'].shares['SWR']).toBe(-4);
      expect(state.players['Alice'].cash).toBe(140);
      expect(state.allCompanies['SWR'].ipoShares).toBe(14);
      expect(state.allCompanies['SWR'].cash).toBe(100);
      expect(state.bank).toBe(660);
    });

    it('Company buy share from IPO', () => {
      state = new ShareTransferredEvent('MR', SHARE_SOURCE.IPO, 'SWR', 3, 20).apply(state);
      expect(state.allCompanies['MR'].shares.hasOwnProperty('SWR')).toBe(true);
      expect(state.allCompanies['MR'].shares['SWR']).toBe(3);
      expect(state.allCompanies['MR'].cash).toBe(40);
      expect(state.allCompanies['SWR'].ipoShares).toBe(7);
      expect(state.allCompanies['SWR'].cash).toBe(100);
      expect(state.bank).toBe(760);
    });

    it('Company sells share to IPO', () => {
      state = new ShareTransferredEvent('MR', SHARE_SOURCE.IPO, 'SWR', -5, 20).apply(state);
      expect(state.allCompanies['MR'].shares.hasOwnProperty('SWR')).toBe(true);
      expect(state.allCompanies['MR'].shares['SWR']).toBe(-5);
      expect(state.allCompanies['MR'].cash).toBe(200);
      expect(state.allCompanies['SWR'].ipoShares).toBe(15);
      expect(state.allCompanies['SWR'].cash).toBe(100);
      expect(state.bank).toBe(600);
    });
  });

  describe('From and to pool', () => {
    beforeEach(() => {
      state = [
        new SettingsChangedEvent(1000, true, true, true, '$', SHARE_SOURCE.POOL, false, true, true, true),
        new PlayerAddedEvent('Alice', 100),
        new CompanyAddedEvent('SWR', 100, 10, '', '', SHARE_SOURCE.POOL),
        new CompanyAddedEvent('MR', 100, 10, '', '', SHARE_SOURCE.POOL)
      ].reduce((s, event) => event.apply(s), new GameState());
    });

    it('Player buys share from pool', () => {
      state = new ShareTransferredEvent('Alice', SHARE_SOURCE.POOL, 'SWR', 2, 15).apply(state);
      expect(state.players['Alice'].shares.hasOwnProperty('SWR')).toBe(true);
      expect(state.players['Alice'].shares['SWR']).toBe(2);
      expect(state.players['Alice'].cash).toBe(70);
      expect(state.allCompanies['SWR'].poolShares).toBe(8);
      expect(state.allCompanies['SWR'].cash).toBe(100);
      expect(state.bank).toBe(730);
    });

    it('Player sells share to pool', () => {
      state = new ShareTransferredEvent('Alice', SHARE_SOURCE.POOL, 'SWR', -3, 15).apply(state);
      expect(state.players['Alice'].shares.hasOwnProperty('SWR')).toBe(true);
      expect(state.players['Alice'].shares['SWR']).toBe(-3);
      expect(state.players['Alice'].cash).toBe(145);
      expect(state.allCompanies['SWR'].poolShares).toBe(13);
      expect(state.allCompanies['SWR'].cash).toBe(100);
      expect(state.bank).toBe(655);
    });

    it('Company buys share from pool', () => {
      state = new ShareTransferredEvent('MR', SHARE_SOURCE.POOL, 'SWR', 4, 20).apply(state);
      expect(state.allCompanies['MR'].shares.hasOwnProperty('SWR')).toBe(true);
      expect(state.allCompanies['MR'].shares['SWR']).toBe(4);
      expect(state.allCompanies['MR'].cash).toBe(20);
      expect(state.allCompanies['SWR'].poolShares).toBe(6);
      expect(state.allCompanies['SWR'].cash).toBe(100);
      expect(state.bank).toBe(780);
    });

    it('Company sells share to pool', () => {
      state = new ShareTransferredEvent('MR', SHARE_SOURCE.POOL, 'SWR', -3, 20).apply(state);
      expect(state.allCompanies['MR'].shares.hasOwnProperty('SWR')).toBe(true);
      expect(state.allCompanies['MR'].shares['SWR']).toBe(-3);
      expect(state.allCompanies['MR'].cash).toBe(160);
      expect(state.allCompanies['SWR'].poolShares).toBe(13);
      expect(state.allCompanies['SWR'].cash).toBe(100);
      expect(state.bank).toBe(640);
    });
  });

  describe('From and to treasury', () => {
    beforeEach(() => {
      state = [
      new SettingsChangedEvent(1000, true, true, true, '$', SHARE_SOURCE.POOL, false, true, true, true),
        new PlayerAddedEvent('Alice', 100),
        new CompanyAddedEvent('NBR', 100, 10, '', '', SHARE_SOURCE.TREASURY),
        new CompanyAddedEvent('SECR', 100, 10, '', '', SHARE_SOURCE.TREASURY)
        ].reduce((s, event) => event.apply(s), new GameState());
    });

    it('Player buying from treasury', () => {
      state = new ShareTransferredEvent('Alice', 'NBR', 'NBR', 1, 25).apply(state);
      expect(state.players['Alice'].shares.hasOwnProperty('NBR')).toBe(true);
      expect(state.players['Alice'].shares['NBR']).toBe(1);
      expect(state.players['Alice'].cash).toBe(75);
      expect(state.allCompanies['NBR'].shares['NBR']).toBe(9);
      expect(state.allCompanies['NBR'].cash).toBe(125);
      expect(state.bank).toBe(700);
    });

    it('Player selling to treasury', () => {
      state = new ShareTransferredEvent('Alice', 'NBR', 'NBR', -2, 25).apply(state);
      expect(state.players['Alice'].shares.hasOwnProperty('NBR')).toBe(true);
      expect(state.players['Alice'].shares['NBR']).toBe(-2);
      expect(state.players['Alice'].cash).toBe(150);
      expect(state.allCompanies['NBR'].shares['NBR']).toBe(12);
      expect(state.allCompanies['NBR'].cash).toBe(50);
      expect(state.bank).toBe(700);
    });

    it('Company buying from treasury', () => {
      state = new ShareTransferredEvent('SECR', 'NBR', 'NBR', 3, 5).apply(state);
      expect(state.allCompanies['SECR'].shares.hasOwnProperty('NBR')).toBe(true);
      expect(state.allCompanies['SECR'].shares['NBR']).toBe(3);
      expect(state.allCompanies['SECR'].cash).toBe(85);
      expect(state.allCompanies['NBR'].shares['NBR']).toBe(7);
      expect(state.allCompanies['NBR'].cash).toBe(115);
      expect(state.bank).toBe(700);
    });

    it('Company selling to treasury', () => {
      state = new ShareTransferredEvent('SECR', 'NBR', 'NBR', -4, 5).apply(state);
      expect(state.allCompanies['SECR'].shares.hasOwnProperty('NBR')).toBe(true);
      expect(state.allCompanies['SECR'].shares['NBR']).toBe(-4);
      expect(state.allCompanies['SECR'].cash).toBe(120);
      expect(state.allCompanies['NBR'].shares['NBR']).toBe(14);
      expect(state.allCompanies['NBR'].cash).toBe(80);
      expect(state.bank).toBe(700);
    });
  });
});
