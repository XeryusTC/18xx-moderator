import { SHARE_SOURCE } from './events';

export class GameState {
  bankSize: number = 0;
  settings: Settings = null;

  players: { [key: string]: Player } = {};
  private companies: { [key: string]: Company } = {};
  privates: { [key: string]: Private } = {};

  get bank(): number {
    let sum = this.bankSize;
    for (const name in this.players) {
      if (this.players.hasOwnProperty(name)) {
        sum -= this.players[name].cash;
      }
    }
    for (const name in this.companies) {
      if (this.companies.hasOwnProperty(name)) {
        sum -= this.companies[name].cash;
      }
    }
    return sum;
  }

  get activeCompanies(): {[key: string]: Company} {
    const result = {};
    for (const company of Object.entries(this.companies)) {
      if (company[1].active) {
        result[company[0]] = company[1];
      }
    }
    return result;
  }

  get disabledCompanies(): {[key: string]: Company} {
    const result = {};
    for (const company of Object.entries(this.companies)) {
      if (!company[1].active) {
        result[company[0]] = company[1];
      }
    }
    return result;
  }

  get allCompanies(): {[key: string]: Company} {
    return this.companies;
  }

  upsertCompany(name: string, company: Company) {
    this.companies[name] = company;
  }

  replaceCompanies(newCompanies: {[key: string]: Company}) {
    this.companies = newCompanies;
  }

  clone(): GameState {
    const newState = new GameState();
    newState.bankSize = this.bankSize;
    if (this.settings) {
      newState.settings = this.settings.clone();
    }
    for (const name in this.players) {
      if (this.players.hasOwnProperty(name)) {
        newState.players[name] = this.players[name].clone();
      }
    }
    for (const name in this.companies) {
      if (this.companies.hasOwnProperty(name)) {
        newState.companies[name] = this.companies[name].clone();
      }
    }
    for (const name in this.privates) {
      if (this.privates.hasOwnProperty(name)) {
        newState.privates[name] = this.privates[name].clone();
      }
    }
    return newState;
  }
}

export class Player {
  cash: number;
  shares: {[key: string]: number} = {};

  clone(): Player {
    const newPlayer = new Player();
    newPlayer.cash = this.cash;
    newPlayer.shares = { ...this.shares };
    return newPlayer;
  }
}

export class Company {
  cash: number;
  shareCount: number;
  textColor: string;
  backgroundColor: string;
  ipoShares: number = 0;
  poolShares: number = 0;
  shares: {[key: string]: number} = {};

  // Store value and revenue so that we can (p)re-fill forms with the last known value
  value: number = null;
  revenues: number[] = [];

  // UI helpers
  active: boolean = true;

  clone(): Company {
    const newCompany = new Company();
    newCompany.cash = this.cash;
    newCompany.shareCount = this.shareCount;
    newCompany.textColor = this.textColor;
    newCompany.backgroundColor = this.backgroundColor;
    newCompany.ipoShares = this.ipoShares;
    newCompany.poolShares = this.poolShares;
    newCompany.shares = { ...this.shares };

    newCompany.value = this.value;
    newCompany.revenues = this.revenues.slice();

    newCompany.active = this.active;
    return newCompany;
  }

  totalRevenue(): number {
    return this.revenues.reduce((a, b) => a + b, 0);
  }
}

export class Private {
  revenue: number;
  active: boolean = true;
  owner: string = null;

  clone(): Private {
    const newPrivate = new Private();
    newPrivate.revenue = this.revenue;
    newPrivate.active = this.active;
    newPrivate.owner = this.owner;
    return newPrivate;
  }
}

export class Settings {
  constructor(public ipoPaysDividend: boolean,
              public poolPaysDividend: boolean,
              public treasuryPaysDividend: boolean,
              public currencySymbol: string,
              public defaultShareSource: SHARE_SOURCE,
              public disableIPO: boolean,
              public companiesCanOwnOtherCompanies: boolean,
              public shortingAllowed: boolean,
              public canSplitRevenue: boolean) { }

  clone(): Settings {
    return Object.create(this);
  }
}
