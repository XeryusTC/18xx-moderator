import { Company, GameState, Player, Private, Settings } from './state';

export enum EVENT_TYPE {
  COMPANY_ADDED = 'CompanyAdded',
  COMPANY_EDITED = 'CompanyEdited',
  COMPANY_REMOVED = 'RemovedCompany',
  COMPOUND = 'Compound',
  MONEY_TRANSFERRED = 'MoneyTransferred',
  OPERATED = 'Operated',
  PLAYER_ADDED = 'PlayerAdded',
  PRIVATE_ADDED = 'PrivateAdded',
  PRIVATE_REMOVED = 'PrivateRemoved',
  PRIVATE_ACQUIRED = 'PrivateAcquired',
  PRIVATE_REVENUE = 'PrivateRevenue',
  SETTINGS_CHANGED = 'SettingsChanged',
  SHARE_TRANSFERRED = 'ShareTransferred',
  UNDONE = 'Undone'
}

export enum SHARE_SOURCE {
  IPO = 'IPO',
  POOL = 'pool',
  TREASURY = 'treasury'
}

export enum DIVIDEND_METHOD {
  FULL = 'FULL',
  SPLIT = 'SPLIT',
  WITHHOLD = 'WITHHOLD'
}

export interface GameEvent {
  readonly type: EVENT_TYPE;
  readonly version: number;
  number: number;
  undone: boolean;
  created: Date;

  apply(state: GameState): GameState;
}

export function convertEventTypeToEventClass(eventType: EVENT_TYPE): GameEvent {
  let type;
  switch (eventType) {
    case EVENT_TYPE.COMPANY_ADDED:     type = CompanyAddedEvent; break;
    case EVENT_TYPE.COMPANY_EDITED:    type = CompanyEditedEvent; break;
    case EVENT_TYPE.COMPANY_REMOVED:   type = CompanyRemovedEvent; break;
    case EVENT_TYPE.COMPOUND:          type = CompoundEvent; break;
    case EVENT_TYPE.MONEY_TRANSFERRED: type = MoneyTransferredEvent; break;
    case EVENT_TYPE.OPERATED:          type = OperatedEvent; break;
    case EVENT_TYPE.PLAYER_ADDED:      type = PlayerAddedEvent; break;
    case EVENT_TYPE.PRIVATE_ACQUIRED:  type = PrivateAcquiredEvent; break;
    case EVENT_TYPE.PRIVATE_ADDED:     type = PrivateAddedEvent; break;
    case EVENT_TYPE.PRIVATE_REMOVED:   type = PrivateRemovedEvent; break;
    case EVENT_TYPE.PRIVATE_REVENUE:   type = PrivateRevenueEvent; break;
    case EVENT_TYPE.SETTINGS_CHANGED:  type = SettingsChangedEvent; break;
    case EVENT_TYPE.SHARE_TRANSFERRED: type = ShareTransferredEvent; break;
    case EVENT_TYPE.UNDONE:            type = UndoneEvent; break;
    default:
      throw new TypeError('Failed to convert');
  }
  return type;
}

export class SettingsChangedEvent implements GameEvent {
  readonly type = EVENT_TYPE.SETTINGS_CHANGED;
  number;
  undone = false;
  created;

  constructor(public readonly bankSize,
              public readonly ipoPaysDividend,
              public readonly poolPaysDividend,
              public readonly treasuryPaysDividend,
              public readonly currencySymbol,
              public readonly defaultShareSource: SHARE_SOURCE,
              public readonly disableIPO: boolean,
              public readonly companiesCanOwnOtherCompanies: boolean,
              public readonly shortingAllowed: boolean,
              public readonly canSplitRevenue: boolean,
              public readonly version = 3) { }

  static fromEventStore(obj: Object) {
    return new SettingsChangedEvent(obj['bankSize'],
      obj['ipoPaysDividend'],
      obj['poolPaysDividend'],
      obj['treasuryPaysDividend'],
      obj['version'] < 1 ? '$' : obj['currencySymbol'],
      obj['version'] < 2 ? SHARE_SOURCE.IPO : obj['defaultShareSource'],
      obj['version'] < 2 ? false : obj['disableIPO'],
      obj['version'] < 2 ? true : obj['companiesCanOwnOtherCompanies'],
      obj['version'] < 2 ? true : obj['shortingAllowed'],
      obj['version'] < 3 ? true : obj['canSplitRevenue'],
      obj['version']);
  }

  apply(state: GameState): GameState {
    const newState = state.clone();
    newState.bankSize = this.bankSize;
    newState.settings = new Settings(this.ipoPaysDividend,
      this.poolPaysDividend,
      this.treasuryPaysDividend,
      this.currencySymbol,
      this.defaultShareSource,
      this.disableIPO,
      this.companiesCanOwnOtherCompanies,
      this.shortingAllowed,
      this.canSplitRevenue,
    );
    return newState;
  }
}

export class PlayerAddedEvent implements GameEvent {
  readonly type = EVENT_TYPE.PLAYER_ADDED;
  number;
  undone = false;
  created;

  constructor(public readonly name: string,
              public readonly startingCash: number,
              public readonly version = 0) { }

  static fromEventStore(obj: Object) {
    return new PlayerAddedEvent(obj['name'], obj['startingCash'], obj['version']);
  }

  apply(state: GameState): GameState {
    // Don't do anything when the player already exists
    if (state.players.hasOwnProperty(this.name)) {
      return state.clone();
    }

    const newState = state.clone();
    const player = new Player();
    player.cash = this.startingCash;
    newState.players[this.name] = player;
    return newState;
  }
}

export class CompanyAddedEvent implements GameEvent {
  readonly type = EVENT_TYPE.COMPANY_ADDED;
  number;
  undone = false;
  created;

  constructor(public readonly name: string,
              public readonly startingCash: number,
              public readonly shares: number,
              public readonly textColor: string,
              public readonly backgroundColor: string,
              public readonly sharesLocation: SHARE_SOURCE,
              public readonly version = 2) { }

  static fromEventStore(obj: Object) {
    return new CompanyAddedEvent(obj['name'],
      obj['startingCash'],
      obj['shares'],
      obj['textColor'],
      obj['backgroundColor'],
      obj['sharesLocation'],
      obj['version']);
  }

  apply(state: GameState): GameState {
    // Do not add the company if it already exists and is active in the state
    if (state.activeCompanies.hasOwnProperty(this.name)) {
      return state.clone();
    }
    // Reactive a company if it was removed
    const newState = state.clone();
    const company = new Company();
    company.cash = this.startingCash;
    company.shareCount = this.shares;
    company.textColor = this.textColor;
    company.backgroundColor = this.backgroundColor;
    company.active = true;
    switch (this.sharesLocation) {
      case SHARE_SOURCE.IPO:
        company.ipoShares = this.shares;
        break;
      case SHARE_SOURCE.POOL:
        company.poolShares = this.shares;
        break;
      case SHARE_SOURCE.TREASURY:
      default:
        company.shares[this.name] = this.shares;
    }
    newState.upsertCompany(this.name, company);
    return newState;
  }
}

export class PrivateAddedEvent implements GameEvent {
  readonly type = EVENT_TYPE.PRIVATE_ADDED;
  number;
  undone = false;
  created;

  constructor(
    public readonly name: string,
    public readonly revenue: number,
    public readonly version = 0
  ) { }

  static fromEventStore(obj: Object) {
    return new PrivateAddedEvent(obj['name'], obj['revenue'], obj['version']);
  }

  apply(state: GameState): GameState {
    // If there is already with a private we do not need to create it
    if (state.privates.hasOwnProperty(this.name)) {
      return state.clone();
    }
    // Create the private
    const newState = state.clone();
    const priv = new Private();
    priv.revenue = this.revenue;
    newState.privates[this.name] = priv;
    return newState;
  }
}

export class CompanyRemovedEvent implements GameEvent {
  readonly type = EVENT_TYPE.COMPANY_REMOVED;
  number;
  undone = false;
  created;

  constructor(public readonly company: string,
              public readonly version = 0) { }

  static fromEventStore(obj: Object) {
    return new CompanyRemovedEvent(obj['company'], obj['version']);
  }

  apply(state: GameState): GameState {
    let newState = state.clone();
    const players = newState.players;
    const companies = newState.allCompanies;
    // Remove all shares from the players
    for (const player in newState.players) {
      if (players.hasOwnProperty(player) && players[player].shares.hasOwnProperty(this.company)) {
        delete newState.players[player].shares[this.company];
      }
    }
    // Remove all shares from the companies
    for (const company in companies) {
      if (companies.hasOwnProperty(company) && companies[company].shares.hasOwnProperty(this.company)) {
        delete companies[company].shares[this.company];
      }
    }
    // Place all shares that the company may still own into the pool
    for (const share in companies[this.company].shares) {
      if (companies[this.company].shares.hasOwnProperty(share) && companies[this.company].shares[share] > 0) {
        companies[share].poolShares += companies[this.company].shares[share];
      }
    }
    companies[this.company].shares = {};
    // Mark the company as disabled
    companies[this.company].active = false;
    newState.replaceCompanies(companies);
    return newState;
  }
}

export class PrivateRemovedEvent implements GameEvent {
  readonly type = EVENT_TYPE.PRIVATE_REMOVED;
  number;
  undone = false;
  created;

  constructor(
    public readonly name: string,
    public readonly version = 0
  ) { }

  static fromEventStore(obj: Object) {
    return new PrivateRemovedEvent(obj['name'], obj['version']);
  }

  apply(state: GameState): GameState {
    const newState = state.clone();
    if (newState.privates.hasOwnProperty(this.name)) {
      newState.privates[this.name].active = false;
    }
    return newState;
  }
}

export class CompanyEditedEvent implements GameEvent {
  readonly type = EVENT_TYPE.COMPANY_EDITED;
  number;
  undone = false;
  created;

  constructor(public readonly name: string,
              public readonly shareCount: number,
              public readonly shareSource: SHARE_SOURCE,
              public readonly textColor: string,
              public readonly backgroundColor: string,
              public readonly version = 0) { }

  static fromEventStore(obj: Object) {
    return new CompanyEditedEvent(
      obj['name'],
      obj['shareCount'],
      obj['shareSource'],
      obj['textColor'],
      obj['backgroundColor'],
      obj['version']);
  }

  apply(state: GameState): GameState {
    const newState = state.clone();
    const company = newState.allCompanies[this.name].clone();
    company.textColor = this.textColor;
    company.backgroundColor = this.backgroundColor;
    if (company.shareCount !== this.shareCount) {
      const diff = this.shareCount - company.shareCount;
      company.shareCount = this.shareCount;
      switch (this.shareSource) {
        case SHARE_SOURCE.POOL:
          company.poolShares += diff;
          break;
        case SHARE_SOURCE.IPO:
          company.ipoShares += diff;
          break;
        case SHARE_SOURCE.TREASURY:
        default:
          if (!company.shares.hasOwnProperty(this.name)) {
            company.shares[this.name] = 0;
          }
          company.shares[this.name] += diff;
          break;
      }
    }
    newState.upsertCompany(this.name, company);
    console.log(newState);
    return newState;
  }
}

export class MoneyTransferredEvent implements GameEvent {
  readonly type = EVENT_TYPE.MONEY_TRANSFERRED;
  number;
  undone = false;
  created;

  constructor(public readonly from: string,
              public readonly to: string,
              public readonly amount: number,
              public readonly reason: string = '',
              public readonly version = 0) { }

  static fromEventStore(obj: Object) {
    return new MoneyTransferredEvent(obj['from'], obj['to'], obj['amount'], obj['reason'], obj['version']);
  }

  apply(state: GameState): GameState {
    const newState = state.clone();
    if (newState.players.hasOwnProperty(this.from)) {
      newState.players[this.from].cash -= this.amount;
    } else if (newState.allCompanies.hasOwnProperty(this.from) ) {
      newState.allCompanies[this.from].cash -= this.amount;
    }

    if (newState.players.hasOwnProperty(this.to)) {
      newState.players[this.to].cash += this.amount;
    } else if (newState.allCompanies.hasOwnProperty(this.to)) {
      newState.allCompanies[this.to].cash += this.amount;
    }
    return newState;
  }
}

export class ShareTransferredEvent implements GameEvent {
  public readonly type = EVENT_TYPE.SHARE_TRANSFERRED;
  number;
  undone = false;
  created;

  constructor(public readonly buyer: string,
              public readonly seller: string,
              public readonly company: string,
              public readonly amount: number,
              public readonly price: number,
              public readonly version = 0) { }

  static fromEventStore(obj: Object) {
    return new ShareTransferredEvent(
      obj['buyer'],
      obj['seller'],
      obj['company'],
      obj['amount'],
      obj['price'],
      obj['version']);
  }

  apply(state: GameState, updateCompanyValue = true): GameState {
    const newState = state.clone();
    // Modify the buyer
    if (this.buyer === SHARE_SOURCE.IPO) {
      newState.allCompanies[this.company].ipoShares += this.amount;
    } else if (this.buyer === SHARE_SOURCE.POOL) {
      newState.allCompanies[this.company].poolShares += this.amount;
    } else if (newState.players.hasOwnProperty(this.buyer)) {
      if (!newState.players[this.buyer].shares.hasOwnProperty(this.company)) {
        newState.players[this.buyer].shares[this.company] = 0; // Ensure that there is a record for the shares
      }
      newState.players[this.buyer].shares[this.company] += this.amount;
      newState.players[this.buyer].cash -= this.amount * this.price;
    } else if (newState.allCompanies.hasOwnProperty(this.buyer)) {
      const buyer = newState.allCompanies[this.buyer];
      if (!buyer.shares.hasOwnProperty(this.company)) {
        buyer.shares[this.company] = 0; // Ensure that there is a record for the shares
      }
      buyer.shares[this.company] += this.amount;
      buyer.cash -= this.amount * this.price;
      newState.upsertCompany(this.buyer, buyer);
    }

    // Modify the seller
    if (this.seller === SHARE_SOURCE.IPO) {
      newState.allCompanies[this.company].ipoShares -= this.amount;
    } else if (this.seller === SHARE_SOURCE.POOL) {
      const company = newState.allCompanies[this.company];
      company.poolShares -= this.amount;
      newState.upsertCompany(this.company, company);
    } else if (newState.players.hasOwnProperty(this.seller)) {
      if (!newState.players[this.seller].shares.hasOwnProperty(this.company)) {
        newState.players[this.seller].shares[this.company] = 0;
      }
      newState.players[this.seller].shares[this.company] -= this.amount;
      newState.players[this.seller].cash += this.amount * this.price;
    } else if (newState.allCompanies.hasOwnProperty(this.seller)) {
      if (!newState.allCompanies[this.seller].shares.hasOwnProperty(this.company)) {
        newState.allCompanies[this.seller].shares[this.company] = 0;
      }
      newState.allCompanies[this.seller].shares[this.company] -= this.amount;
      newState.allCompanies[this.seller].cash += this.amount * this.price;
    }

    // Modify the stock value of the company
    if (updateCompanyValue) {
      newState.allCompanies[this.company].value = this.price;
    }

    return newState;
  }
}

export class PrivateAcquiredEvent implements GameEvent {
  public readonly type = EVENT_TYPE.PRIVATE_ACQUIRED;
  number;
  undone = false;
  created;

  constructor(
    public readonly name,
    public readonly buyer,
    public readonly price,
    public readonly version = 0
  ) { }

  static fromEventStore(obj: Object) {
    return new PrivateAcquiredEvent(obj['name'], obj['buyer'], obj['price'], obj['version']);
  }

  apply(state: GameState): GameState {
    const newState = state.clone();
    const owner = newState.privates[this.name].owner;
    // Increase capital of the original owner
    if (newState.players.hasOwnProperty(owner)) {
      newState.players[owner].cash += this.price;
    } else if (newState.allCompanies.hasOwnProperty(owner)) {
      newState.allCompanies[owner].cash += this.price;
    }
    // Change owner
    newState.privates[this.name].owner = this.buyer;
    // Decrease cash of the new owner
    if (newState.players.hasOwnProperty(this.buyer)) {
      newState.players[this.buyer].cash -= this.price;
    } else if (newState.allCompanies.hasOwnProperty(this.buyer)) {
      newState.allCompanies[this.buyer].cash -= this.price;
    }
    return newState;
  }
}

export class PrivateRevenueEvent implements GameEvent {
  public readonly type = EVENT_TYPE.PRIVATE_REVENUE;
  number;
  undone = false;
  created;

  constructor(
    public readonly name: string,
    public readonly version = 0
  ) { }

  static fromEventStore(obj: Object) {
    return new PrivateRevenueEvent(obj['name'], obj['version']);
  }

  apply(state: GameState): GameState {
    const newState = state.clone();
    const owner = newState.privates[this.name].owner;
    if (newState.players.hasOwnProperty(owner)) {
      newState.players[owner].cash += newState.privates[this.name].revenue;
    } else if (newState.allCompanies.hasOwnProperty(owner)) {
      newState.allCompanies[owner].cash += newState.privates[this.name].revenue;
    }
    return newState;
  }
}

export class OperatedEvent implements GameEvent {
  public readonly type = EVENT_TYPE.OPERATED;
  number;
  undone = false;
  created;

  constructor(public readonly company: string,
              public readonly revenues: number[],
              public readonly method: DIVIDEND_METHOD,
              public readonly version = 1) { }

  static fromEventStore(obj: Object) {
    if (obj['version'] === 0) {
      return new OperatedEvent(obj['company'], [obj['amount']], obj['method'], obj['version']);
    } else {
      return new OperatedEvent(obj['company'], obj['revenues'], obj['method'], obj['version']);
    }
  }

  apply(state: GameState): GameState {
    const newState = state.clone();
    // If the company has been removed then it does not operate
    if (!newState.activeCompanies.hasOwnProperty(this.company)) {
      return newState;
    }
    const company = newState.allCompanies[this.company];

    // Find the total revenue
    const amount = this.revenues.reduce((a, b) => a + b, 0);
    company.revenues = this.revenues;

    if (this.method === DIVIDEND_METHOD.WITHHOLD) {
      company.cash += amount;
    } else {
      let dividendPerShare: number;

      // Splitting is complicated because 10-share companies generally have different rules than smaller companies
      if (this.method === DIVIDEND_METHOD.SPLIT) {
        if (company.shareCount === 10) {
          dividendPerShare = Math.ceil(amount / 20);
          company.cash += amount - dividendPerShare * 10;
        } else {
          dividendPerShare = amount / 2 / company.shareCount;
          company.cash += amount / 2;
        }
      } else if (this.method === DIVIDEND_METHOD.FULL) {
        dividendPerShare = amount / company.shareCount;
      }

      // Do the actual payout
      if (newState.settings.ipoPaysDividend) {
        company.cash += company.ipoShares * dividendPerShare;
      }
      if (newState.settings.poolPaysDividend) {
        company.cash += company.poolShares * dividendPerShare;
      }
      // Pay out to the companies (if required)
      if (newState.settings.treasuryPaysDividend) {
        for (const name in newState.allCompanies) {
          if (newState.allCompanies.hasOwnProperty(name)) {
            const otherCompany = newState.allCompanies[name];
            if (otherCompany.shares.hasOwnProperty(this.company)) {
              otherCompany.cash += otherCompany.shares[this.company] * dividendPerShare;
            }
          }
        }
      }
      // Pay out to the players
      for (const player in newState.players) {
        if (newState.players[player].shares.hasOwnProperty(this.company)) {
          newState.players[player].cash += newState.players[player].shares[this.company] * dividendPerShare;
        }
      }
    }
    return newState;
  }
}

export class UndoneEvent implements GameEvent {
  public readonly type = EVENT_TYPE.UNDONE;
  number;
  undone = false;
  created;

  constructor(public readonly eventNumber: number,
              public readonly version = 0) { }

  static fromEventStore(obj: Object) {
    return new UndoneEvent(obj['eventNumber'], obj['version']);
  }

  apply(state: GameState): GameState {
    // This action doesn't do anything by itself, just return a copy of the state
    return state.clone();
  }
}

export class CompoundEvent implements GameEvent {
  readonly type = EVENT_TYPE.COMPOUND;
  number;
  undone = false;
  created;

  constructor(public readonly title: string,
              public readonly events: GameEvent[],
              public readonly version = 0) { }

  static fromEventStore(obj: Object) {
    const events = obj['events'].map(event => {
      let type;
      try {
        type = convertEventTypeToEventClass(event['type']);
      } catch (e) {
        console.log('Failed to convert event that is part of a compound event', event);
      }
      return type.fromEventStore(event);
    });
    return new CompoundEvent(obj['title'], events, obj['version']);
  }

  apply(state: GameState): GameState {
    return this.events.reduce((nextState, event) => event.apply(nextState), state);
  }
}
