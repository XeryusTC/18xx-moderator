import { GameState } from './state';
import {
  CompanyAddedEvent,
  DIVIDEND_METHOD,
  GameEvent,
  OperatedEvent,
  PlayerAddedEvent,
  PrivateAddedEvent,
  SettingsChangedEvent,
  SHARE_SOURCE,
  ShareTransferredEvent
} from './events';

function apply(state: GameState, event: GameEvent) {
  return event.apply(state);
}

describe('GameState', () => {
  let state: GameState = new GameState();
  it('should return the amount remaining in the bank', () => {
    state = [
      new SettingsChangedEvent(1000, true, true, true, '', SHARE_SOURCE.IPO, true, true, true, true),
      new PlayerAddedEvent('Alice', 100),
      new PlayerAddedEvent('Bob', 75),
    ].reduce(apply, new GameState());
    expect(state.bank).toBe(825);
    state = [
      new CompanyAddedEvent('NYC', 37, 1, '', '', SHARE_SOURCE.IPO),
      new CompanyAddedEvent('B&O', 42, 1, '', '', SHARE_SOURCE.IPO),
      new PlayerAddedEvent('Charlie', 25)
    ].reduce(apply, state);
    expect(state.bank).toBe(721);
  });

  it('should clone the size of the bank', () => {
    state = [
      new SettingsChangedEvent(10,true, true, true, '', SHARE_SOURCE.IPO, true, true, true, true)
    ].reduce(apply, new GameState());
    const newState = state.clone();
    expect(newState).not.toBe(state);
    expect(newState.bankSize).toEqual(state.bankSize);
    expect(newState.bankSize).toBe(10);
  });

  it('should clone the settings', () => {
    state.settings = jasmine.createSpyObj('Settings', ['clone']);
    expect(state.settings.clone).not.toHaveBeenCalled();
    state.clone();
    expect(state.settings.clone).toHaveBeenCalledTimes(1);
  });

  it('should clone the players', () => {
    state.players = {
      'Dave': jasmine.createSpyObj('Player', ['clone']),
      'Eve': jasmine.createSpyObj('Player', ['clone'])
    };
    expect(state.players['Dave'].clone).not.toHaveBeenCalled();
    expect(state.players['Eve'].clone).not.toHaveBeenCalled();
    const newState = state.clone();
    expect(state.players['Dave'].clone).toHaveBeenCalledTimes(1);
    expect(state.players['Eve'].clone).toHaveBeenCalledTimes(1);
    expect(newState.players['Dave']).not.toBe(state.players['Dave']);
    expect(newState.players['Eve']).not.toBe(state.players['Eve']);
  });

  it('should clone the companies', () => {
    state.replaceCompanies({
      'B&M': jasmine.createSpyObj('Company', ['clone']),
      'CPR': jasmine.createSpyObj('Company', ['clone'])
    });
    expect(state.allCompanies['B&M'].clone).not.toHaveBeenCalled();
    expect(state.allCompanies['CPR'].clone).not.toHaveBeenCalled();
    const newState = state.clone();
    expect(state.allCompanies['B&M'].clone).toHaveBeenCalledTimes(1);
    expect(state.allCompanies['CPR'].clone).toHaveBeenCalledTimes(1);
    expect(newState.allCompanies['B&M']).not.toBe(state.allCompanies['B&M']);
    expect(newState.allCompanies['CPR']).not.toBe(state.allCompanies['CPR']);
  });

  it('should clone the privates', () => {
    state.privates = {
      'M&H': jasmine.createSpyObj('Private', ['clone']),
      'B&O': jasmine.createSpyObj('Private', ['clone'])
    };
    expect(state.privates['M&H'].clone).not.toHaveBeenCalled();
    expect(state.privates['B&O'].clone).not.toHaveBeenCalled();
    const newState = state.clone();
    expect(state.privates['M&H'].clone).toHaveBeenCalledTimes(1);
    expect(state.privates['B&O'].clone).toHaveBeenCalledTimes(1);
    expect(newState.privates['M&H']).not.toBe(state.privates['M&H']);
    expect(newState.privates['B&O']).not.toBe(state.privates['B&O']);
  });
});

describe('Player', () => {
  it('should clone itself', () => {
    const state = new PlayerAddedEvent('Alice', 83).apply(new GameState());
    const playerBefore = state.players['Alice'];
    const playerAfter = playerBefore.clone();
    expect(playerAfter).not.toBe(playerBefore);
    expect(playerAfter.cash).toBe(playerBefore.cash);
    expect(playerAfter.cash).toBe(83);
  });

  it('should clone its shares', () => {
    const state = [
      new PlayerAddedEvent('Bob', 0),
      new CompanyAddedEvent('NYC', 0, 10, '', '', SHARE_SOURCE.IPO),
      new CompanyAddedEvent('NNH', 0, 10, '', '', SHARE_SOURCE.IPO),
      new ShareTransferredEvent('Bob', SHARE_SOURCE.IPO, 'NYC', 3, 0),
      new ShareTransferredEvent('Bob', SHARE_SOURCE.IPO, 'NNH', 5, 0)
    ].reduce(apply, new GameState());
    const playerBefore = state.players['Bob'];
    const playerAfter = playerBefore.clone();
    expect(playerAfter.shares).not.toBe(playerBefore.shares);
    expect(playerAfter.shares).toEqual(playerBefore.shares);
    expect(playerAfter.shares['NYC']).toBe(3);
    expect(playerAfter.shares['NNH']).toBe(5);
  });
});

describe('Company', () => {
  it('should clone itself', () => {
    const state = new CompanyAddedEvent('CPR', 654, 10, '#123', '#abc', SHARE_SOURCE.IPO).apply(new GameState());
    const companyBefore = state.allCompanies['CPR'];
    const companyAfter = companyBefore.clone();
    expect(companyAfter).not.toBe(companyBefore);
    expect(companyAfter.active).toBe(companyBefore.active);
    expect(companyAfter.active).toBe(true);
    expect(companyAfter.cash).toBe(companyBefore.cash);
    expect(companyAfter.cash).toBe(654);
    expect(companyAfter.ipoShares).toBe(companyBefore.ipoShares);
    expect(companyAfter.ipoShares).toBe(10);
    expect(companyAfter.shareCount).toBe(companyBefore.shareCount);
    expect(companyAfter.shareCount).toBe(10);
    expect(companyAfter.textColor).toBe(companyBefore.textColor);
    expect(companyAfter.textColor).toBe('#123');
    expect(companyAfter.backgroundColor).toBe(companyBefore.backgroundColor);
    expect(companyAfter.backgroundColor).toBe('#abc');
    expect(companyAfter.poolShares).toBe(companyBefore.poolShares);
    expect(companyAfter.poolShares).toBe(0);
    expect(companyAfter.value).toBe(companyBefore.value);
    expect(companyAfter.value).toBe(null);
  });

  it('should clone its shares', () => {
    const state = [
      new CompanyAddedEvent('C&O', 0, 10, '', '', SHARE_SOURCE.TREASURY),
      new CompanyAddedEvent('PRR', 0, 10, '', '', SHARE_SOURCE.IPO),
      new ShareTransferredEvent('C&O', SHARE_SOURCE.IPO, 'PRR', 2, 0),
      new ShareTransferredEvent('PRR', 'C&O', 'C&O', 4, 0)
    ].reduce(apply, new GameState());
    const companyBefore = state.allCompanies['C&O'];
    const companyAfter = companyBefore.clone();
    expect(companyAfter).not.toBe(companyBefore);
    expect(companyAfter.shares).toEqual(companyBefore.shares);
    expect(companyAfter.shares['C&O']).toBe(6);
    expect(companyAfter.shares['PRR']).toBe(2);
  });

  it('should clone its revenues', () => {
    const state = [
      new SettingsChangedEvent(100, true, true, true, '', SHARE_SOURCE.IPO, true, true, true, true),
      new CompanyAddedEvent('Erie', 0, 10, '', '', SHARE_SOURCE.IPO),
      new OperatedEvent('Erie', [10, 20, 30], DIVIDEND_METHOD.FULL),
    ].reduce(apply, new GameState());
    const companyBefore = state.allCompanies['Erie'];
    const companyAfter = companyBefore.clone();
    expect(companyAfter).not.toBe(companyBefore);
    expect(companyAfter.revenues).toEqual(companyBefore.revenues);
    expect(companyAfter.revenues).toEqual([10, 20, 30]);
    expect(companyAfter.totalRevenue()).toBe(60);
  });
});

describe('Private', () => {
  it('should clone itself', () => {
    const state = new PrivateAddedEvent('C&A', 25).apply(new GameState());
    const privateBefore = state.privates['C&A'];
    const privateAfter = privateBefore.clone();
    expect(privateAfter).not.toBe(privateBefore);
    expect(privateAfter.revenue).toBe(privateBefore.revenue);
    expect(privateAfter.revenue).toBe(25);
    expect(privateAfter.active).toBe(privateBefore.active);
    expect(privateAfter.active).toBe(true);
    expect(privateAfter.owner).toBe(privateBefore.owner);
    expect(privateAfter.owner).toBeNull();
  });
});

describe('Settings', () => {
  it('should clone itself', () => {
    const state = new SettingsChangedEvent(1200, false, true, false, '$', SHARE_SOURCE.POOL, false, false, false, true).apply(new  GameState());
    const settingsBefore = state.settings;
    const settingsAfter = settingsBefore.clone();
    expect(settingsAfter).not.toBe(settingsBefore);
    expect(settingsAfter.ipoPaysDividend).toBe(settingsBefore.ipoPaysDividend);
    expect(settingsAfter.ipoPaysDividend).toBe(false);
    expect(settingsAfter.poolPaysDividend).toBe(settingsBefore.poolPaysDividend);
    expect(settingsAfter.poolPaysDividend).toBe(true);
    expect(settingsAfter.treasuryPaysDividend).toBe(settingsBefore.treasuryPaysDividend);
    expect(settingsAfter.treasuryPaysDividend).toBe(false);
    expect(settingsAfter.currencySymbol).toBe(settingsBefore.currencySymbol);
    expect(settingsAfter.currencySymbol).toBe('$');
    expect(settingsAfter.defaultShareSource).toBe(settingsBefore.defaultShareSource);
    expect(settingsAfter.defaultShareSource).toBe(SHARE_SOURCE.POOL);
    expect(settingsAfter.disableIPO).toBe(settingsBefore.disableIPO);
    expect(settingsAfter.disableIPO).toBe(false);
    expect(settingsAfter.companiesCanOwnOtherCompanies).toBe(settingsBefore.companiesCanOwnOtherCompanies);
    expect(settingsAfter.companiesCanOwnOtherCompanies).toBe(false);
    expect(settingsAfter.shortingAllowed).toBe(settingsBefore.shortingAllowed);
    expect(settingsAfter.shortingAllowed).toBe(false);
    expect(settingsAfter.canSplitRevenue).toBe(settingsBefore.canSplitRevenue);
    expect(settingsAfter.canSplitRevenue).toBe(true);
  })
});
