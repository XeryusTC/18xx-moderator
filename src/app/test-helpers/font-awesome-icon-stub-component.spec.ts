/* tslint:disable */
import { Component, Input } from '@angular/core';

@Component({ selector: 'fa-icon', template: '' })
export class FontAwesomeIconStubComponent {
  @Input() icon;
}

