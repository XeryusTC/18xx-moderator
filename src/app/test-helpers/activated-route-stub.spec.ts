import { Params } from '@angular/router';
import { ReplaySubject } from 'rxjs';

/**
 * An ActivatedRoute test double with a `params` observable.
 */
export class ActivatedRouteStub {
  // Use a ReplaySubject to share previous values with subscribers and pump new values into the `params` observable
  private paramsSubject = new ReplaySubject<Params>();
  private parentParamsSubject = new ReplaySubject<Params>();

  constructor(initialParams?: Params, parentParams?: Params) {
    this.setParams(initialParams);
    this.setParentParams(parentParams);

    this.params = this.paramsSubject.asObservable();
    this.parentParams = this.paramsSubject.asObservable();
    this.parent = { params: this.parentParams };
  }

  // The mock params
  readonly params = this.paramsSubject.asObservable();
  readonly parentParams;
  readonly parent;

  // Set the next params observable value
  setParams(params?: Params) {
    this.paramsSubject.next(params);
  }

  setParentParams(params?: Params) {
    this.parentParamsSubject.next(params);
  }
}
