/* tslint:disable */
import { Directive, Input } from '@angular/core';

// @ts-ignore
@Directive({
  selector: '[routerLink]',
  host: { '(click)': 'onClick()' }
})
export class RouterLinkDirectiveStub {
  @Input('routerLink') linkParams: any;
  @Input('routerLinkActiveOptions') options: any;
  navigatedTo: any = null;

  onClick() {
    this.navigatedTo = this.linkParams;
  }
}
